require 'test_helper'

class SubempresasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @subempresa = subempresas(:one)
  end

  test "should get index" do
    get subempresas_url
    assert_response :success
  end

  test "should get new" do
    get new_subempresa_url
    assert_response :success
  end

  test "should create subempresa" do
    assert_difference('Subempresa.count') do
      post subempresas_url, params: { subempresa: { comuna_id: @subempresa.comuna_id, direccion: @subempresa.direccion, giro: @subempresa.giro, imagen: @subempresa.imagen, razon_social: @subempresa.razon_social, rut: @subempresa.rut } }
    end

    assert_redirected_to subempresa_url(Subempresa.last)
  end

  test "should show subempresa" do
    get subempresa_url(@subempresa)
    assert_response :success
  end

  test "should get edit" do
    get edit_subempresa_url(@subempresa)
    assert_response :success
  end

  test "should update subempresa" do
    patch subempresa_url(@subempresa), params: { subempresa: { comuna_id: @subempresa.comuna_id, direccion: @subempresa.direccion, giro: @subempresa.giro, imagen: @subempresa.imagen, razon_social: @subempresa.razon_social, rut: @subempresa.rut } }
    assert_redirected_to subempresa_url(@subempresa)
  end

  test "should destroy subempresa" do
    assert_difference('Subempresa.count', -1) do
      delete subempresa_url(@subempresa)
    end

    assert_redirected_to subempresas_url
  end
end
