require 'test_helper'

class TipoSectorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tipo_sector = tipo_sectors(:one)
  end

  test "should get index" do
    get tipo_sectors_url
    assert_response :success
  end

  test "should get new" do
    get new_tipo_sector_url
    assert_response :success
  end

  test "should create tipo_sector" do
    assert_difference('TipoSector.count') do
      post tipo_sectors_url, params: { tipo_sector: { nombre: @tipo_sector.nombre } }
    end

    assert_redirected_to tipo_sector_url(TipoSector.last)
  end

  test "should show tipo_sector" do
    get tipo_sector_url(@tipo_sector)
    assert_response :success
  end

  test "should get edit" do
    get edit_tipo_sector_url(@tipo_sector)
    assert_response :success
  end

  test "should update tipo_sector" do
    patch tipo_sector_url(@tipo_sector), params: { tipo_sector: { nombre: @tipo_sector.nombre } }
    assert_redirected_to tipo_sector_url(@tipo_sector)
  end

  test "should destroy tipo_sector" do
    assert_difference('TipoSector.count', -1) do
      delete tipo_sector_url(@tipo_sector)
    end

    assert_redirected_to tipo_sectors_url
  end
end
