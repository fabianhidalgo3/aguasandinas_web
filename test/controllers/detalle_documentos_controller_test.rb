require 'test_helper'

class DetalleDocumentosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @detalle_documento = detalle_documentos(:one)
  end

  test "should get index" do
    get detalle_documentos_url
    assert_response :success
  end

  test "should get new" do
    get new_detalle_documento_url
    assert_response :success
  end

  test "should create detalle_documento" do
    assert_difference('DetalleDocumento.count') do
      post detalle_documentos_url, params: { detalle_documento: { detalle: @detalle_documento.detalle, documento_id: @detalle_documento.documento_id, monto: @detalle_documento.monto } }
    end

    assert_redirected_to detalle_documento_url(DetalleDocumento.last)
  end

  test "should show detalle_documento" do
    get detalle_documento_url(@detalle_documento)
    assert_response :success
  end

  test "should get edit" do
    get edit_detalle_documento_url(@detalle_documento)
    assert_response :success
  end

  test "should update detalle_documento" do
    patch detalle_documento_url(@detalle_documento), params: { detalle_documento: { detalle: @detalle_documento.detalle, documento_id: @detalle_documento.documento_id, monto: @detalle_documento.monto } }
    assert_redirected_to detalle_documento_url(@detalle_documento)
  end

  test "should destroy detalle_documento" do
    assert_difference('DetalleDocumento.count', -1) do
      delete detalle_documento_url(@detalle_documento)
    end

    assert_redirected_to detalle_documentos_url
  end
end
