require 'test_helper'

class TipoConsumosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tipo_consumo = tipo_consumos(:one)
  end

  test "should get index" do
    get tipo_consumos_url
    assert_response :success
  end

  test "should get new" do
    get new_tipo_consumo_url
    assert_response :success
  end

  test "should create tipo_consumo" do
    assert_difference('TipoConsumo.count') do
      post tipo_consumos_url, params: { tipo_consumo: { descripcion: @tipo_consumo.descripcion, nombre: @tipo_consumo.nombre } }
    end

    assert_redirected_to tipo_consumo_url(TipoConsumo.last)
  end

  test "should show tipo_consumo" do
    get tipo_consumo_url(@tipo_consumo)
    assert_response :success
  end

  test "should get edit" do
    get edit_tipo_consumo_url(@tipo_consumo)
    assert_response :success
  end

  test "should update tipo_consumo" do
    patch tipo_consumo_url(@tipo_consumo), params: { tipo_consumo: { descripcion: @tipo_consumo.descripcion, nombre: @tipo_consumo.nombre } }
    assert_redirected_to tipo_consumo_url(@tipo_consumo)
  end

  test "should destroy tipo_consumo" do
    assert_difference('TipoConsumo.count', -1) do
      delete tipo_consumo_url(@tipo_consumo)
    end

    assert_redirected_to tipo_consumos_url
  end
end
