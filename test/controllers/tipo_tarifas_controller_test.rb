require 'test_helper'

class TipoTarifasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tipo_tarifa = tipo_tarifas(:one)
  end

  test "should get index" do
    get tipo_tarifas_url
    assert_response :success
  end

  test "should get new" do
    get new_tipo_tarifa_url
    assert_response :success
  end

  test "should create tipo_tarifa" do
    assert_difference('TipoTarifa.count') do
      post tipo_tarifas_url, params: { tipo_tarifa: { facturacion_en_terreno: @tipo_tarifa.facturacion_en_terreno, nombre: @tipo_tarifa.nombre } }
    end

    assert_redirected_to tipo_tarifa_url(TipoTarifa.last)
  end

  test "should show tipo_tarifa" do
    get tipo_tarifa_url(@tipo_tarifa)
    assert_response :success
  end

  test "should get edit" do
    get edit_tipo_tarifa_url(@tipo_tarifa)
    assert_response :success
  end

  test "should update tipo_tarifa" do
    patch tipo_tarifa_url(@tipo_tarifa), params: { tipo_tarifa: { facturacion_en_terreno: @tipo_tarifa.facturacion_en_terreno, nombre: @tipo_tarifa.nombre } }
    assert_redirected_to tipo_tarifa_url(@tipo_tarifa)
  end

  test "should destroy tipo_tarifa" do
    assert_difference('TipoTarifa.count', -1) do
      delete tipo_tarifa_url(@tipo_tarifa)
    end

    assert_redirected_to tipo_tarifas_url
  end
end
