module ApplicationHelper

	def flash_class(level)
		case level
		when 'notice' then "alert alert-info"
		when 'success' then "alert alert-success"
		when 'error' then "alert alert-danger"
		when 'alert' then "alert alert-warning"
		end
  end

	def javascript(*files)
  		content_for(:head) { javascript_include_tag(*files) }
	end

	def stylesheet(*files)
  		content_for(:head) { stylesheet_link_tag(*files) }
	end

end
