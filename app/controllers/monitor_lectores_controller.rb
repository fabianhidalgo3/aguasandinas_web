class MonitorLectoresController < ApplicationController
  respond_to :js, :html, :json

	def index
		user = current_usuario
    zonas = user.empleado.zona
    @regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
  end

  # GET
  # Retorna lista con zonas
  def carga_zonas
    user = current_usuario
    empleado = user.empleado
    @zonas = Zona.where(region_id: params[:region]).joins(:empleado).where(empleados: {id: empleado.id})
    respond_with @zonas
  end

  # GET
  # Muestra todas las comunas con ordenes en estado 1, 2
  def carga_comunas
    @comunas = Comuna.where(zona_id: params[:zona]).joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [1..2]}).group(:id).order(:nombre)
    respond_with @comunas
  end

  # GET
  # Retorna lista con porciones
  def carga_porciones
    @porciones = Porcion.where(zona_id: params[:zona], abierto: true).joins(:asignacion => [:porcion]).where(porcions:{abierto:true}).group(:id).order(:codigo)
    respond_with @porciones
  end

  # GET
  # Retorna lista con rutas
  def carga_rutas
    if !params[:comuna].blank?
      @rutas = Rutum.where(porcion_id: params[:porcion]).joins(:orden_lectura).where(orden_lecturas: {comuna_id: params[:comuna], estado_lectura_id:[2..5]}).group(:id)
    else
      @rutas = Rutum.where(porcion_id: params[:porcion].to_i)
    end
    respond_with @rutas
  end

  # GET
  # Retorna lista con empleados
  def carga_lectores
    @lectores = Empleado.joins(:usuario, :zona).where(usuarios:{perfil_id: [6,7]}, zonas:{id: params[:zona]})
    respond_with @lectores
  end

  # GET
  # Devuelve @hash con ubicaciones
  def carga_ubicacion
    ubicaciones = Array.new
    detalles = DetalleOrdenLectura.where("detalle_orden_lecturas.fecha_ejecucion IS NOT NULL").joins(:orden_lectura => [:asignacion]).where(
      orden_lecturas: {rutum_id: params[:ruta_id], estado_lectura_id: [4,5]},
        asignacions: {empleado_id: params[:emp_id]}
    ).order("detalle_orden_lecturas.fecha_ejecucion ASC").group("orden_lecturas.id")
      detalles.each do |detalle|        
        p fecha_ejecucion = detalle.fecha_ejecucion
        ventana = "N Cliente: " + detalle.orden_lectura.cliente.numero_cliente.to_s + "\n" +
        detalle.fecha_ejecucion.strftime('%e/%m/%Y %I:%M:%S %p')
        ubicaciones.push([detalle.orden_lectura.gps_latitud, detalle.orden_lectura.gps_longitud, ventana])
      end
    @hash = Gmaps4rails.build_markers(ubicaciones) do |u, marker|
      marker.lat u[0]
      marker.lng u[1]
      marker.infowindow u[2]
    end
    respond_with @hash
  end

  # GET
  # Contenido de la tabla
  def carga_filtro
    lista = Array.new
    @totales = Array.new
    tAsignado = 0, tCargado = 0, tPendiente = 0, tEfectivo = 0, tNoEfectivo = 0, tLeido = 0, tVerificaciones = 0, tEfectividad = 0
    asignado = 0, cargado = 0, efectivo = 0, noEfectivo = 0, pendientes = 0, leido = 0, ol_repaso = 0, efectividad = 0

    contador = 0
    porciones = Porcion.where(zona_id: params[:zona],abierto:true)
    rutas = Rutum.where(porcion_id: params[:porcion])
    if rutas.blank?
      rutas = Rutum.joins(:porcion).where(porcions: {zona_id: params[:zona], abierto:true})
    end
    empleados = Empleado.joins(:usuario, :asignacion).where(usuarios: {perfil_id: [6,7]}, asignacions: {porcion_id: porciones}).group(:id)
    empleados.each do |empleado|
      asignaciones = Asignacion.where(
                    empleado_id: empleado.id).joins(:porcion, :orden_lectura).where(porcions:{ id: porciones },
                    orden_lecturas:{estado_lectura_id: [2..5], rutum_id: rutas}).group(:rutum_id)
      asignaciones.each do |asignacion|
        asignado = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [2,3,4,5], relectura: false})
        cargado = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [3], relectura: false}).count
        asignado.each do |a|
          if !a.orden_lectura.detalle_orden_lectura.first.clave_lectura.nil? && a.orden_lectura.detalle_orden_lectura.first.clave_lectura.efectivo 
            efectivo = efectivo + 1
          end
          if !a.orden_lectura.detalle_orden_lectura.first.clave_lectura.nil? && !a.orden_lectura.detalle_orden_lectura.first.clave_lectura.efectivo 
            noEfectivo = noEfectivo + 1 
          end
        end
        asignado = asignado.count 
        pendientes = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [2,3], relectura: false}).count
        leido = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: 4, relectura: false}).count
        ol_repaso = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura]).where(orden_lecturas: {tipo_lectura_id: [2], estado_lectura_id: [1..3], relectura: false}).count
        efectividad = ((efectivo*100.0)/asignado).round(2)
        hora = OrdenLectura.where(rutum_id: asignacion.rutum_id, estado_lectura_id: [4,5], relectura:false).joins(:asignacion, :detalle_orden_lectura).where(asignacions: {empleado_id: asignacion.empleado_id}).order("detalle_orden_lecturas.fecha_ejecucion ASC")
        if !hora.blank? then
          horaInicio = hora.first.detalle_orden_lectura.first.fecha_ejecucion.strftime("%H:%M:%S %d/%m/%Y")
          horaTermino = hora.last.detalle_orden_lectura.first.fecha_ejecucion.strftime("%H:%M:%S %d/%m/%Y")
        else
          horaInicio = " "
          horaTermino = " "
        end
        transmitido = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(
                      :orden_lectura).where(orden_lecturas: {estado_lectura_id: 5, relectura: false}).count
                      
        lista.push([empleado.nombre_completo, asignacion.orden_lectura.rutum.codigo, asignado,
                    cargado, efectivo, noEfectivo, pendientes, leido, efectividad, horaInicio, horaTermino,
                    transmitido, asignacion.rutum_id, asignacion.empleado_id, ol_repaso, 
                    asignacion.orden_lectura.rutum.porcion.codigo])
      asignado = 0, cargado = 0, efectivo = 0, noEfectivo = 0, pendientes = 0, leido = 0, ol_repaso = 0, efectividad = 0          
      end
      
    end
    lista.sort! {|a,b| a[1] <=> b[1]}
		@lista = lista.paginate(:page => params[:page], :per_page => 7)
    respond_with @lista
  end
end
