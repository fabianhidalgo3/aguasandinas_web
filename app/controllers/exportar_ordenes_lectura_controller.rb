class ExportarOrdenesLecturaController < ApplicationController

  def index
    @rutas = Rutum.where(abierto: false)
  end

  def exportar_csv
    listaOrdenes = Array.new
    porcion = 1
    rutas = Rutum.where(porcion_id: porcion)
    #ruta = Rutum.find(params[:ruta])
    detalleLecturas = DetalleOrdenLectura.joins(:orden_lectura).where(orden_lecturas: {rutum_id: rutas, relectura:false})
    detalleLecturas.each do |detalle|
      if (detalle.lectura_actual == 0)
        lecturaActual = ""
      else
        lecturaActual = detalle.lectura_actual
      end
  
      if !detalle.clave_lectura.nil?
        claveLectura = detalle.clave_lectura.nombre
      else
        claveLectura = ""
      end
    
      if !detalle.fecha_ejecucion.blank?
        fecha_ejecucion = detalle.fecha_ejecucion.strftime('%e/%m/%Y %I:%M:%S %p')
      else
        fecha_efecucion = ""
      end
        
      fotografia = Fotografium.where(detalle_orden_lectura_id: detalle.id).first
      if !fotografia.nil?
        fotografia = fotografia.archivo
      else
        fotografia = ""
      end
      listaOrdenes.push([detalle.orden_lectura.medidor.numero_medidor,
                        detalle.orden_lectura.rutum.porcion.codigo,
                        detalle.orden_lectura.cliente.numero_cliente,
                        detalle.tipo_consumo.nombre,
                        fecha_ejecucion,
                        detalle.orden_lectura.orden_mandante,
                        lecturaActual,
                        claveLectura,
                        detalle.orden_lectura.tipo_lectura.nombre,
                        detalle.orden_lectura.gps_latitud,
                        detalle.orden_lectura.gps_longitud,
                        fotografia])
      end


    cabecera = "Medidor, Ciclo, Producto, Tipo Consumo, Fecha Lectura,Codigo Orden,Lectura,Clave Lectura , Tipo Lectura,GPS Latitud, GPS Longitud, Fotografia\n"
    fechaDescarga = Time.now.strftime("%d-%m-%Y_%H%M%S").to_s
    nombreArchivo = "ciclo5_"+fechaDescarga+".csv"
    File.open(nombreArchivo, "w+:UTF-16LE:UTF-8") do |fila|
      fila << cabecera
      listaOrdenes.each do |l|
        fila << l[0].to_s + "," + l[1].to_s + "," + l[2].to_s + "," + l[3].to_s + "," + l[4].to_s + "," + l[5].to_s + "," + l[6].to_s +
            + "," + l[7].to_s + "," + l[8].to_s + "," + l[9].to_s + "," + l[10].to_s + "," + l[11].to_s + "\n"
      end
    end
    send_file(nombreArchivo, x_sendfile: true, buffer_size: 512)
  end
end
