class ReporteEfectividadPorcionController < ApplicationController
	respond_to :js, :html, :json, :pdf

	def index # > Pagina Principal
		user = current_usuario	# > Busca usuario y zonas y porcion del usuario logueado
    @zonas = user.empleado.zona
    @tipo_lectura = TipoLectura.all
		@claves = ClaveLectura.all
		@totales = Array.new
	end

	# => Carga Porciones
	def carga_porciones 			
		zona = params[:zona].to_i
		@porciones = Porcion.where(zona_id: zona).group(:mes).order(:mes)
		respond_with @porciones
	end
	
	# > Funcion encargada de buscar ordenes..
	# > Params[porcion]
	def carga_filtro 			
		@lista = Array.new
		@totales = Array.new
		t_asignado = 0
		t_claveEfectivo = 0
		t_claveImprocedente = 0
		t_problemasTecnicos = 0
		asignado = 0
		claveEfectivo = 0
		claveImprocedente = 0
		problemasTecnicos = 0
		total = 0
		t_leidos = 0
		contador = 0
		efectividad = 0
		detalles = 0
		tDetalles = 0
		mes = Porcion.find(params[:porcion]).mes
		porcion = Porcion.where(mes: mes)
		p porcion.count
		listaEmpleados = Empleado.joins(:usuario).where(usuarios: {perfil_id: [6,7]})		
	 	listaEmpleados.each do |empleado|
			listaAsignaciones = Asignacion.where(empleado_id: empleado.id).joins(:porcion,:orden_lectura).where(porcions:{ id: porcion },orden_lecturas:{estado_lectura_id: [2..5]}).group(:rutum_id)
		
		 if !listaAsignaciones.blank?
			 listaAsignaciones.each do |asignacion|
					ruta = asignacion.rutum
				 	comuna = asignacion.orden_lectura.comuna.nombre
				 	asignado = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas:{relectura:false, estado_lectura_id:[2..5]}).count
					leidos = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas:{relectura:false, estado_lectura_id: [4..5]}).count
					detalles = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [2..5], relectura:false}).count 
					
					claveEfectivo = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5], relectura:false}, clave_lecturas: {efectivo: true}).count
					claveImprocedente = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5], relectura:false}, clave_lecturas: {efectivo:false}).count
					 
				 	problemasTecnicos = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5], relectura:false}, clave_lecturas: {id: [6,7,11,12,14,19,21,22,23]}).count
					totalLecturas = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura]).where(orden_lecturas: {estado_lectura_id: [2..5], relectura: false}).count 
					efectividad = ((claveEfectivo*100.0)/totalLecturas).round(2)
					
					@lista.push([ruta.porcion.codigo, comuna, empleado.nombre_completo, 
				 							 ruta.codigo, asignado, claveEfectivo, claveImprocedente, problemasTecnicos, 
											 (claveEfectivo + claveImprocedente),efectividad, detalles])
					
					t_asignado = t_asignado + asignado	
					t_claveEfectivo = t_claveEfectivo + claveEfectivo
					t_claveImprocedente = t_claveImprocedente + claveImprocedente
					t_problemasTecnicos = t_problemasTecnicos + problemasTecnicos	
					tDetalles = tDetalles + detalles
					total = total + (claveEfectivo + claveImprocedente)
					t_leidos = t_leidos + leidos
				end
		 end
	end

	t_efectividad = ((t_claveEfectivo*100.0)/tDetalles).round(2)
	 @totales.push(t_asignado, t_claveEfectivo, t_claveImprocedente, t_problemasTecnicos, total, t_efectividad, tDetalles, t_leidos)
	 # > Ordena Lista por Nombre Ruta
	 @lista.sort! {|a,b| a[3] <=> b[3]}
	 respond_with @lista
	end

end
