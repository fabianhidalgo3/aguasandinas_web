class OperacionesController < ApplicationController
  respond_to :js, :html

  def ver_ubicacion 
    ubicaciones = Array.new
		lectura = OrdenLectura.where(id: params[:orden_id]).first
    if (!lectura.gps_latitud.nil? && !lectura.gps_longitud.nil?) && (lectura.gps_latitud.to_i != 0  && lectura.gps_longitud.to_i != 0)
    	detalle = lectura.detalle_orden_lectura.take#DetalleOrdenLectura.where(orden_lectura_id: a.id).first
      ventana = "N Cliente: " + lectura.cliente.numero_cliente.to_s + "\n" + detalle.fecha_ejecucion.strftime('%e/%m/%Y %I:%M:%S %p')
      ubicaciones.push([lectura.gps_latitud, lectura.gps_longitud, ventana])
    end
    @hash = Gmaps4rails.build_markers(ubicaciones) do |u, marker|
      marker.lat u[0]
      marker.lng u[1]
      marker.infowindow u[2]
		end
		respond_with @hash
  end

  def ver_fotografia
    detalle = DetalleOrdenLectura.where(orden_lectura_id: params[:id_orden]).first
    @numero_cliente = detalle.orden_lectura.cliente.numero_cliente
    @lista_fotos = Fotografium.joins(:detalle_orden_lectura => [:orden_lectura]).where(orden_lecturas: {id:params[:id_orden]})
    respond_with @lista_fotos
  end

  # GET
  # Devuelve hash con ubicación del recinto
  def ver_ubicacion_cliente
    ubicaciones = Array.new
    cliente = Cliente.find(params[:cliente])
    @cliente = cliente
    @clienteID = cliente.id
    if (!cliente.gps_latitud.nil? && !cliente.gps_longitud.nil?) && (cliente.gps_latitud.to_i != 0  && cliente.gps_longitud.to_i != 0)
      ventana = cliente.nombre.to_s + " | " + cliente.direccion
      ubicaciones.push([cliente.gps_latitud.to_s, cliente.gps_longitud.to_s, ventana])
      @latitud = cliente.gps_latitud.to_s
      @longitud = cliente.gps_longitud.to_s
    end
    @hash = Gmaps4rails.build_markers(ubicaciones) do |u, marker|
      marker.lat u[0]
      marker.lng u[1]
      marker.infowindow u[2]
    end
		respond_with @hash
  end

  def actualizar_ubicacion
    cliente = Cliente.find(params[:cliente])
    cliente.update(gps_latitud: params[:latitud], gps_longitud: params[:longitud], observacion_recinto: params[:observacion], verificado: true)
    cliente.save
  end

  def remplazar_ubicacion
    cliente = Cliente.find(params[:cliente])
    ultimaOrden = cliente.orden_lectura.last
    if (!ultimaOrden.gps_latitud.nil? && !ultimaOrden.gps_longitud.nil?)
      cliente.update(gps_latitud: ultimaOrden.gps_latitud, gps_longitud: ultimaOrden.gps_longitud, verificado:1)
      cliente.save
    end
  end



end