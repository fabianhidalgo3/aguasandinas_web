class BienvenidaController < ApplicationController
	respond_to :js

	def index
		#carpetaClientes()
		user = current_usuario
		zonas = user.empleado.zona
		@porciones = Porcion.where(abierto: true).group(:codigo)
		rutas = Rutum.where(porcion_id: @porciones)
		lecturas= OrdenLectura.where(relectura: false, tipo_lectura_id: 1, estado_lectura_id: 4).joins(:rutum =>[:porcion]).where(porcions: {abierto: true})
		contador = 0
		contadorUno = 0
		lecturas.each do |l|
			p l.id
			if l.detalle_orden_lectura.first.clave_lectura.efectivo	
				contador = contador +1
			end
			if !l.detalle_orden_lectura.first.clave_lectura.efectivo	
				contadorUno = contadorUno +1
			end
		end
			@lEfectivas = contador
			@lImprocedentes = contadorUno
			@fLeido = contador + contadorUno
	end

	def dashboard
		@verificaciones = OrdenLectura.where(relectura: false, tipo_lectura_id: 2, estado_lectura_id: [1..3]).count
	end
	
	require 'fileutils'

	def carpetaClientes
		clientes = Cliente.all
		clientes.each do |cliente|
			FileUtils.mkdir cliente.numero_cliente.to_s
		end
	end




	# GET
	# Grafico Completitud
	def completitud
		porcionIds = Array.new
		valores = params[:valoresCheck]
		valores = valores.split(",")
		valores.each do |valor|
			porcionCodigo = Porcion.find(valor.to_i).codigo
			porciones = Porcion.where(codigo: porcionCodigo, abierto: true)
			porciones.each do |p|
				porcionIds.push(p.id)
			end
		end
		@porcionGraficos = Porcion.where(id: porcionIds, abierto: true).group(:zona_id)

		productividadValparaiso = @porcionGraficos.where(zona_id: 1, abierto: true)
		@AsignadoValparaiso = OrdenLectura.where(estado_lectura_id: [2,3,4,5], relectura: false).joins(:rutum).where(ruta: {porcion_id: productividadValparaiso }).count
		@SinAsignarValparaiso = OrdenLectura.where(estado_lectura_id: 1, relectura: false).joins(:rutum).where(ruta: {porcion_id: productividadValparaiso} ).count

	end

	# GET
	# Grafico Productividad
	def productividad
		porcionIds = Array.new
		valores = params[:valoresCheck]
		valores = valores.split(",")
		valores.each do |valor|
			porcionCodigo = Porcion.find(valor.to_i).codigo
			porciones = Porcion.where(codigo: porcionCodigo, abierto: true)
			porciones.each do |p|
				porcionIds.push(p.id)
			end
		end
		@porcionGraficos = Porcion.where(id: porcionIds).group(:zona_id)

		productividadValparaiso = @porcionGraficos.where(zona_id: 1,  abierto: true)
		@AsignadoProducValparaiso = OrdenLectura.where(estado_lectura_id: [2,3], relectura: false).joins(:rutum).where(ruta: {porcion_id: productividadValparaiso }).count
		@LeidoValparaiso = OrdenLectura.where(estado_lectura_id: [4,5], relectura: false).joins(:rutum).where(ruta: {porcion_id: productividadValparaiso} ).count

	end 

	# GET
	# Grafico Efectividad
	def efectividad
		porcionIds = Array.new
		valores = params[:valoresCheck]
		valores = valores.split(",")
		valores.each do |valor|
			porcionCodigo = Porcion.find(valor.to_i).codigo
			porciones = Porcion.where(codigo: porcionCodigo)
			porciones.each do |p|
				porcionIds.push(p.id)
			end
		end
		@porcionGraficos = Porcion.where(id: porcionIds, abierto: true).group(:zona_id)
		p "porciones Efectividad"
		p @porcionGraficos

		efectividadValparaiso = @porcionGraficos.where(zona_id: 1, abierto: true)
		efValparaiso = OrdenLectura.where(estado_lectura_id: [4,5], relectura: false).joins(:rutum).where(ruta: {porcion_id: efectividadValparaiso})
		contador = 0
		efValparaiso.each do |ef|
			if ef.detalle_orden_lectura.first.clave_lectura.efectivo
				contador = contador +1
			else
				p "Error"
			end
		end
		p "contador"
		p contador



		@EfectividadValparaiso = contador

		@LeidoEfecValparaiso = OrdenLectura.where(estado_lectura_id: [4,5], relectura: false).joins(:rutum).where(ruta: {porcion_id: efectividadValparaiso} ).count

	end 


	def clientes_dudosos
		porcionIds = Array.new
		valores = params[:valoresCheck]
		valores = valores.split(",")
		valores.each do |valor|
			porcionCodigo = Porcion.find(valor.to_i).codigo
			porciones = Porcion.where(codigo: porcionCodigo,abierto: true)
			porciones.each do |p|
				porcionIds.push(p.id)
			end
		end
		@porcionGraficos = Porcion.where(id: porcionIds, abierto: true).group(:zona_id)
		
		dudososValparaiso = @porcionGraficos.where(zona_id: 1, abierto: true)
		@dudososValparaiso = OrdenLectura.where(estado_lectura_id: 4, autorizado_facturacion: false, relectura: false, consumo_dudoso: true).joins(:detalle_orden_lectura => [:clave_lectura], :rutum => [:porcion]).where(clave_lecturas: {efectivo: true}, porcions: {id: dudososValparaiso}).count
		@LeidoValparaiso = OrdenLectura.where(estado_lectura_id: [4,5], relectura: false).joins(:rutum).where(ruta: {porcion_id: dudososValparaiso} ).count
	end
		
	# > Descargar Manual Aplicación Web
	def descargar_manual_web
		send_file "#{Rails.root}/public/Manuales/web.pdf", type: "application/pdf", x_sendfile: true
	end

	# > Descargar Manual Aplicación Movíl
	def descargar_manual_app
		send_file "#{Rails.root}/public/Manuales/app.pdf", type: "application/pdf", x_sendfile: true
	end
	
end
