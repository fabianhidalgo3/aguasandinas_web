class AjusteUbicacionController < ApplicationController
  respond_to :js
  require 'aws-sdk-s3'

  def index
    @clientes = Cliente.where(verificado: true).paginate(:page => params[:page], :per_page => 50)
    informacion()
    # exportarFotografias()
    #conexionS3()
  
  end
  
  def carga_clientes
    informacion()
    estado = params[:estado].to_i
    if estado == 1
      @clientes = Cliente.where(verificado: true)
    elsif estado == 2 
      @clientes = Cliente.where(verificado: false)
    else
      @clientes = Cliente.all
    end
    @clientes = @clientes.paginate(:page => params[:page], :per_page => 50)
    respond_with @clientes
  end

  def informacion
    clientes = Cliente.all
    @totalClientes = clientes.count
    @clientesVerificados = clientes.where(verificado: true).count
    @clientesSinVerificar = clientes.where(verificado: false).count
  end


  def conexionS3
    s3 = Aws::S3::Resource.new(region:'us-east-1')
    my_bucket = s3.bucket('aguasandinas')
    my_bucket.objects.limit(100).each do |obj|
      p obj
    end
  end

  bucketPrincipal = 'aguasandinas'
  bucketClientes = 'clientes_aguasandinas'

  def exportarFotografias
    bucketPrincipal = 'aguasandinas'
    bucketClientes = 'clientes_aguasandinas'
    s3 = Aws::S3::Resource.new(region:'us-east-1')
    #s3.create_bucket(bucket: bucketClientes)
    clientes = Cliente.all
    clientes.each do |cliente|
      new_path = cliente.numero_cliente
      ordenLectura = OrdenLectura.where(estado_lectura_id: 4, cliente_id: cliente.id)
      if !ordenLectura.blank?
        fotografias = ordenLectura.last.detalle_orden_lectura.first.fotografium
        fotografias.each do |fotografia|
          aws_path = fotografia.archivo.to_s
          p aws_path
          aws_object = AWS::S3::S3Object.find aws_path, 'aguasandinas'
          s3.copy_object(bucket: bucket,copy_source: "#{bucketPrincipal}/fotografias/#{fotografia.archivo}",key: 'file1')
          s3.copy_object(bucket: bucket,copy_source: "#{bucketClientes}/fotografias/#{new_path}",key: 'file2')
        end
        nombreCarpeta = cliente.numero_cliente
      end
    end
  end

end
