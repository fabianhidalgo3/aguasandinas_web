class ReporteVisitasController < ApplicationController
  respond_to :js

  def index
    @porciones = Porcion.all.group(:mes).order(:mes)
  end


  def mostrar_busqueda
    rutas = Rutum.joins(:porcion).where(porcions: {mes: params[:mes]})
    @arrayOrdenes = Array.new
    ordenLecturas = OrdenLectura.where(estado_lectura_id: 4, tipo_lectura_id: 1, rutum_id: rutas).joins(:detalle_orden_lectura).group(:id).order("detalle_orden_lecturas.fecha_ejecucion DESC")
    contadorImprocedente = 0
    contadorEfectivo = 0
    ordenLecturas.each do |o|
      row = Array.new
      if !o.cliente.nil?
        row.push(o.cliente.nombre)
        row.push(o.cliente.numero_cliente)
      else
        row.push("-")
        row.push("-")
      end
      row.push(o.direccion)
      if !o.comuna.nil?
        row.push(o.comuna.nombre)
      else
        row.push("-")
      end
      if !o.rutum.nil? && !o.rutum.porcion.nil? && !o.rutum.porcion.zona.nil?
        row.push(o.rutum.porcion.zona.nombre)
      else
          row.push("-")
      end
      if !o.rutum.nil? && !o.rutum.porcion.nil?
        row.push(o.rutum.porcion.codigo)
      else
          row.push("-")
      end
      if !o.rutum.nil?
        row.push(o.rutum.codigo)
      else
          row.push("-")
      end
      row.push(o.posicion)
      row.push(o.medidor.numero_medidor)
      row.push(o.detalle_orden_lectura.first.lectura_actual)
      row.push(o.factor_cobro.nombre)
      if !o.detalle_orden_lectura.first.clave_lectura.nil?
        if o.detalle_orden_lectura.first.clave_lectura.efectivo
          contadorEfectivo += +1
        else
          contadorImprocedente += +1
        end
        row.push(o.detalle_orden_lectura.first.clave_lectura.nombre)
        row.push(o.detalle_orden_lectura.first.clave_lectura.descripcion_corta)
      else
        row.push("-")
        row.push("-")
      end
      if !o.detalle_orden_lectura.first.observacione.nil?
        row.push(o.detalle_orden_lectura.first.observacione.descripcion)
      else
        row.push("")
      end
      row.push(o.observacion)
      if !o.cliente.nil?
        row.push(o.cliente.observacion_recinto)
      else
        row.push("")
      end

      if !o.asignacion.nil? && !o.asignacion.empleado.nil?
        row.push(o.asignacion.empleado.nombre_completo)
      else
        row.push("-")
      end
      if !o.detalle_orden_lectura.nil?
        row.push(o.detalle_orden_lectura.first.fecha_ejecucion)
      else
        row.push("-")
      end
      if !o.cliente.nil?
        row.push(o.cliente.gps_latitud)
        row.push(o.cliente.gps_longitud)
        row.push(o.cliente.id)
      end
      row.push(o.id)
      row.push(o.detalle_orden_lectura.first.clave_lectura.efectivo)

      @arrayOrdenes.push(row)
    end
    @improcedentes = contadorImprocedente
    @efectivos = contadorEfectivo
    @arrayOrdenes.paginate
    respond_with @arrayOrdenes
  end


  def exportar_csv
    mes = params[:mes].to_i
    if mes > 0
      rutas = Rutum.joins(:porcion).where(porcions: {mes: mes})
    elsif mes = "Todos"
      rutas = Rutum.all
    end 
    p rutas
    ordenLecturas = OrdenLectura.where(estado_lectura_id: [4..5], tipo_lectura_id: 1, rutum_id: rutas).joins(:detalle_orden_lectura).group(:id)
    p ordenLecturas.count
    header = "Codigo Orden,Nº Cliente, Nombre, Dirección, Comuna, Zona, Grupo, Libreta,Lectura, Clave, Correlativo,Medidor, Empleado\n"
    fecha = Time.now.strftime("%d%m%Y%H%M%S").to_s
    file = "reporte - " +  fecha.to_s + ".csv"
    observacion = ""
    File.open(file, "w+:UTF-16LE:UTF-8") do |csv|
      csv << header
      ordenLecturas.each do |l|

        csv << l.codigo.to_s + "," +
               l.cliente.numero_cliente.to_s + "," +
               l.cliente.nombre + ","  +
               l.cliente.direccion + ","  +
               l.comuna.nombre + "," +
               l.rutum.porcion.zona.nombre.to_s + "," +
               l.rutum.porcion.codigo.to_s + "," +
               l.rutum.codigo.to_s + "," +
               l.detalle_orden_lectura.first.lectura_actual.to_s + "," +
               l.detalle_orden_lectura.first.clave_lectura.nombre.to_s + "," +
               l.secuencia_lector.to_s + "," +
               l.medidor.numero_medidor.to_s + "," + 
               l.asignacion.empleado.nombre + "," + "\n"
      end
    end
    send_file(file, x_sendfile: true, buffer_size: 512, disposition: 'attachment')
  end
end
