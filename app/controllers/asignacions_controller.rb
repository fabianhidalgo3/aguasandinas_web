class AsignacionsController < ApplicationController
  respond_to :js, :html, :json

  def index
    user = current_usuario
    @zonas = user.empleado.zona
    @porcion = Porcion.where(zona_id: @zonas)
    @tipo_lectura = TipoLectura.all
    @estados = EstadoLectura.where(id: [1,2])
  end

  # GET
  # PARCIAL: Muestra las porciones
  def carga_porciones
    @porciones = Porcion.where(zona_id: params[:zona], abierto: true).order(:codigo)
    respond_with @porciones
  end

  # GET
  # PARCIAL: Muestra las comunas
  def carga_comunas
    @comunas = Comuna.where(zona_id: params[:zona]).joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [1..2]}).group(:id).order(:nombre)
    respond_with @comunas
  end

  # GET
  # Retorna lista de ordenes asignadas o ordenes con asignación
  def carga_filtro
    estado = params[:Estado]
    comuna = (Comuna.find(params[:comuna]) unless params[:comuna].blank?)
    # Creo Lista Vacia para devolver las ordenes
    @lista = []
    # Ordenes Sin Asignación
    if estado.to_i == 1
      @variable = 0
      zona = Zona.find(params[:zona])
      @empleados = zona.empleado.joins(:usuario).where(usuarios: {perfil_id: [6,7]})
      #Creo lista vacia para las guardar las ordenes
      # Busco todas las rutas sin asignar y que esten abiertas
      rutasSinAsignar = Rutum.where(porcion_id: params[:porcion], abierto: true)
      p rutasSinAsignar
      if rutasSinAsignar.blank?
        rutasSinAsignar = Rutum.joins(:porcion).where(porcions:{abierto: true})
      end
      # Recorro todas las rutas sin asignar
      rutasSinAsignar.each do |ruta|
        #Si no existe la comuna busca toda la ruta de lo contratio buscara solo las ordenes de esa ruta con la comuna seleccionada
        if !comuna.nil?
          ordenes = OrdenLectura.where(rutum_id: ruta.id, tipo_lectura_id: params[:proceso], estado_lectura_id: estado.to_i, comuna_id: comuna.id)
        else
          ordenes = OrdenLectura.where(rutum_id: ruta.id, tipo_lectura_id: params[:proceso], estado_lectura_id: estado.to_i)
        end
        p ordenes
        next unless ordenes.count > 0
        row = []
        row.push(ruta.porcion.codigo)
        row.push(ordenes.first.comuna.nombre)
        row.push(ruta.codigo)
        row.push(ordenes.count)
        row.push('0 Hrs')
        row.push('0 %')
        row.push(ruta.id)
        row.push(ordenes.first.comuna.id)
        row.push(ordenes.joins(:detalle_orden_lectura).count)
        @lista.push(row)
      end
    end

    #Ordenes con Asignación
    if estado.to_i == 2
    	@variable = 1
      zona = Zona.find(params[:zona])
      empleados = zona.empleado.joins(:usuario).where(usuarios: {perfil_id: [6,7]})
      porciones = Porcion.where(id: params[:porcion]).first
      if porciones.blank?
        porciones = Porcion.where(zona_id: zona, abierto: true)
      end
      empleados.each do |empleado|
        #Si no existe la comuna busca toda la ruta de lo contratio buscara solo las ordenes de esa ruta con la comuna seleccionada
        if !comuna.nil?
          asignaciones = Asignacion.where(empleado_id: empleado.id).joins(:porcion, :orden_lectura).where(porcions: {id: porciones },orden_lecturas:{estado_lectura_id: [2,3], tipo_lectura_id: params[:proceso].to_i, comuna_id: comuna.id, relectura: false} ).group(:rutum_id)
        else
          asignaciones = Asignacion.where(empleado_id: empleado.id).joins(:porcion, :orden_lectura).where(porcions: {id: porciones },orden_lecturas:{estado_lectura_id: [2,3], tipo_lectura_id: params[:proceso].to_i, relectura: false} ).group(:rutum_id)
        end

        if !asignaciones.nil?
          asignaciones.each do |asignacion|
            row = []
            row.push(asignacion.porcion.codigo)
            row.push(asignacion.orden_lectura.comuna.nombre)
            row.push(asignacion.orden_lectura.rutum.codigo)
            row.push(Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [2,3], tipo_lectura_id: params[:proceso].to_i, relectura: false}).count)
            row.push("0 Hrs")
            row.push("0 %")
            row.push(empleado.nombre_completo)
            row.push(empleado.id)
            row.push(asignacion.orden_lectura.rutum.id)
            row.push(asignacion.orden_lectura.comuna.id)
            row.push(params[:proceso])
            row.push(Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(orden_lectura: [:detalle_orden_lectura]).where(orden_lecturas: {estado_lectura_id: [2,3],tipo_lectura_id: params[:proceso].to_i, relectura: false}).count)
            @lista.push(row)
          end
        end
      end
    end
    @lista.sort! {|a,b| a[0] <=> b[0]}
    respond_with @lista
  end


  # GET
  # Actualiza un lote prefinido de lecturas
  def asignacion_completa
    ruta = Rutum.where(id: params[:ruta]).first
    if !params[:comuna].blank?
      ordenes = OrdenLectura.where(rutum_id: ruta.id,  estado_lectura_id: 1, comuna_id: params[:comuna], tipo_lectura_id: params[:proceso])
    else
      ordenes = OrdenLectura.where(rutum_id: ruta.id,  estado_lectura_id: 1,  tipo_lectura_id: params[:proceso])
    end
    @totalOrdenes = ordenes.count
    ordenes.each do |orden|
      Asignacion.create(orden_lectura_id: orden.id, empleado_id: params[:empleado], porcion_id: ruta.porcion.id, rutum_id: ruta.id)
      orden.update(estado_lectura_id: 2, fecha_asignacion: Time.zone.now)
    end
    respond_with @totalOrdenes
  end

  # GET
  # Dividir Rutas para asignacion
  def carga_dividir_ruta_asignacion
    @ruta_seleccionada = Rutum.where(id: params[:ruta]).first
    @lector = Empleado.where(id: params[:empleado]).first
    if !params[:comuna].blank?
      @ordenes_lectura = OrdenLectura.where(rutum_id: params[:ruta], estado_lectura_id: 1, comuna_id: params[:comuna]).order(:posicion)
    else
      @ordenes_lectura = OrdenLectura.where(rutum_id: params[:ruta], estado_lectura_id: 1).order(:posicion)
    end
  end

  # GET
  # Guarda las Asignaciónes Parciales
  def asignacion_parcial
    @completado = params[:id_boton_asignacion].to_i
    ruta = Rutum.where(id: params[:ruta]).first
    ordenes = params[:ordenes].split(',')
    ordenes.each do |orden_id|
      orden = OrdenLectura.where(id: orden_id).first
      Asignacion.new(rutum_id: ruta.id, porcion_id: ruta.porcion.id,
                     orden_lectura_id: orden.id, empleado_id: params[:empleado].to_i).save
      orden.update(estado_lectura_id:  2, fecha_asignacion: Time.zone.now)
      orden.save
    end
    @lector_id = Empleado.where(id: params[:empleado].to_i).first.id
    @ruta_seleccionada_id = ruta.id
    @tipo_lectura_id = OrdenLectura.find(ordenes.first).tipo_lectura_id
    respond_with @completado
  end

  # GET
  # Elimina la ruta por comuna o solo comuna..
  def desasignacion_completa
    @completado_dos = 0
    ruta = Rutum.where(id: params[:ruta]).first
    if !params[:comuna].blank?
      ordenes = OrdenLectura.where(rutum_id: ruta.id, estado_lectura_id: [2,3], tipo_lectura_id: params[:tipo_lectura], comuna_id: params[:comuna]).joins(:asignacion).where(asignacions: {empleado_id: params[:empleado]})
    else
      ordenes = OrdenLectura.where(rutum_id: ruta.id, estado_lectura_id: [2,3], tipo_lectura_id: params[:tipo_lectura]).joins(:asignacion).where(asignacions: {empleado_id: params[:empleado]})
    end
    ordenes.each do |o|
      o.asignacion.destroy
      o.update(estado_lectura_id: 1, fecha_asignacion: '')
    end
    respond_with @completado_dos
  end

  # GET
  # Dividir Rutas para Desasignacion
  def carga_dividir_ruta_desasignacion
    @ruta_seleccionada = Rutum.where(id: params[:ruta]).first
    @lector = Empleado.where(id: params[:empleado]).first
    @asignaciones = Asignacion.where(empleado_id: @lector.id, rutum_id: @ruta_seleccionada.id,
                                     porcion_id: @ruta_seleccionada.porcion.id).joins(:orden_lectura).where(
                                     orden_lecturas: {tipo_lectura_id: params[:tipo_lectura], estado_lectura_id: [2,3]})
  end

  # GET
  # Elimina las asignaciones parciales
  def desasignacion_parcial
    @completado_uno = params[:id_boton_desasignacion].to_i
    ruta = Rutum.where(id: params[:ruta]).first
    empleado = params[:empleado]
    ordenes = params[:ordenes].split(',')
    ordenes.each do |orden_id|
      orden = OrdenLectura.find(orden_id)
      orden.asignacion.destroy
      orden.update(estado_lectura_id:1, fecha_asignacion: '')
    end
    @lector_id = Empleado.where(id: params[:empleado].to_i).first.id
    @ruta_seleccionada_id = ruta.id
    @tipo_lectura_id = OrdenLectura.find(ordenes.first).tipo_lectura_id
    respond_with @completado_uno
  end
end
