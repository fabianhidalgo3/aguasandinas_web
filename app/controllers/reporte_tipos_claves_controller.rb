class ReporteTiposClavesController < ApplicationController
	def index # > Pagina Principal
		user = current_usuario	# > Busca usuario y zonas y porcion del usuario logueado
    @zonas = user.empleado.zona
		@claves = ClaveLectura.all
		@totales = Array.new
	end

	respond_to :js, :html, :json
	def carga_porciones 	# => Carga Porciones		
		zona = params[:zona].to_i
		@porciones = Porcion.where(zona_id: zona)
		respond_with @porciones
	end



	respond_to :js, :html, :json
	def carga_filtro 	# => Filtro del Reporte		
		@lista = Array.new
		@totales = Array.new
		t_asignado = 0
		t_claveEfectivo = 0
		t_claveImprocedente = 0
		t_problemasTecnicos = 0
		asignado = 0
		claveEfectivo = 0
		claveImprocedente = 0
		problemasTecnicos = 0
		total = 0
		t_efectividad =0
		contador = 0
		listaEmpleados = Empleado.joins(:usuario).where(usuarios: {perfil_id: [6,7]})		
	 	listaEmpleados.each do |empleado|
			listaAsignaciones = Asignacion.where(empleado_id: empleado.id).joins(:porcion,
				:orden_lectura).where(porcions:{ zona_id: params[:zona].to_i, id: params[:porcion].to_i },
				orden_lecturas:{estado_lectura_id: [2,3,4,5]}).group(:rutum_id)
		
		 if !listaAsignaciones.blank?
			 listaAsignaciones.each do |asignacion|
				 ruta = asignacion.rutum
				 comuna = asignacion.orden_lectura.comuna.nombre
				 asignado = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas:{relectura:false, estado_lectura_id:[2..5]}).count
				 obs1 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:1 }).count
				 obs2 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:2 }).count
				 obs3 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:3 }).count
				 obs4 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:4 }).count
				 obs5 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:5 }).count
				 obs6 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:6 }).count
				 obs7 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:7 }).count
				 obs8 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:8 }).count
				 obs9 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:9 }).count
				 obs10 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:10 }).count
				 obs11 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:11 }).count
				 obs12 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:12 }).count
				 obs13 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:13 }).count
				 obs14 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:14 }).count
				 obs15 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:15 }).count
				 obs16 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:16 }).count
				 obs17 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:17 }).count
				 obs18 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:18 }).count
				 obs19 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:19 }).count
				 obs20 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:20 }).count
				 obs21 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:21 }).count
				 obs22 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:22 }).count
				 obs23 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:23 }).count
				 obs24 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:24 }).count
				 obs25 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:25 }).count
				 obs26 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:26 }).count
				 obs27 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:27 }).count
				 obs28 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:28 }).count
				 obs29 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:29 }).count
				 obs30 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:30 }).count
				 obs31 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:31 }).count
				 obs32 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:32 }).count
				 obs33 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:33 }).count
				 obs34 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:34 }).count
				 obs35 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:35 }).count
				 obs36 = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5],relectura:false}, clave_lecturas: {id:36 }).count
				p obs36
				 @lista.push([ruta.porcion.codigo_nombre, comuna, empleado.nombre_completo, ruta.codigo, asignado,
											 obs1,obs2,obs3,obs4,obs5,obs6,obs7,obs8,obs9,obs10,obs11,obs12,obs13,
											 obs14,obs15,obs16,obs17,obs18,obs19,obs20,obs21,obs22,obs23,obs24,obs25,obs26,
											 obs27,obs28,obs29,obs30,obs31,obs32,obs33,obs34,obs35,obs36])
			 end
			 	contador += contador
				t_asignado +=  asignado	
				t_claveEfectivo = t_claveEfectivo + claveEfectivo
				t_claveImprocedente = t_claveImprocedente + claveImprocedente
				t_problemasTecnicos = t_problemasTecnicos + problemasTecnicos	
				total = total + (claveEfectivo + claveImprocedente + problemasTecnicos)
				if t_efectividad > 0
					t_efectividad = (t_efectividad + (claveEfectivo*100.0)/asignado).round(2)/contador
				end
		 end 
	 end
	 @totales.push(t_asignado, t_claveEfectivo, t_claveImprocedente, t_problemasTecnicos, total, t_efectividad)
	 # > Ordena Lista por Nombre Ruta
	 @lista.sort! {|a,b| a[3] <=> b[3]}
	 respond_with @lista
	end
end
