class CerrarProcesosLecturasController < ApplicationController
	def index
		user = current_usuario
    zonas = user.empleado.zona
    @regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
	end

	# GET
  # Retorna lista con zonas
  def carga_zonas
    user = current_usuario
    empleado = user.empleado
    @zonas = Zona.where(region_id: params[:region]).joins(:empleado).where(empleados: {id: empleado.id})
    respond_with @zonas  
  end

	respond_to :js, :html, :json	
	def carga_filtro 
		@lista = Array.new
		if !params[:zona].blank?
			porciones = Porcion.where(zona_id: params[:zona], abierto: true)
		else
			porciones = Porcion.where(abierto: true)
		end
		porciones.each do |p|
			total_porcion = OrdenLectura.where(relectura:false).joins(:rutum).where(ruta: {porcion_id: p.id}).group(:estado_lectura_id).count
			asignado = OrdenLectura.where(estado_lectura_id: [2..5],relectura:false).joins(:rutum).where(ruta: {porcion_id: p.id}).count
			pendientes = OrdenLectura.where(estado_lectura_id: [1..3], relectura:false).joins(:rutum).where(ruta: {porcion_id: p.id}).count
			leido = OrdenLectura.where(estado_lectura_id: [4], relectura:false).joins(:rutum).where(ruta: {porcion_id: p.id}).count
			total_rutas = Rutum.where(porcion_id: p.id).count
			total_ordenes = OrdenLectura.where(estado_lectura_id: [1..5], relectura:false).joins(:rutum).where(ruta: {porcion_id: p.id}).count
			oficina = p.zona.nombre
			ordenLista = p.zona.id
			@lista.push([p.codigo_nombre,total_rutas, total_ordenes,pendientes,total_porcion[1],asignado,total_porcion[3],total_porcion[4],p.abierto, oficina, ordenLista])
		end	
		@lista.sort! {|a,b| a[10] <=> b[10]}
		respond_with @lista		
	end
end
