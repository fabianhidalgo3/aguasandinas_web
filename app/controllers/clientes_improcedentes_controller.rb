class ClientesImprocedentesController < ApplicationController
  respond_to :js, :html, :json
  def index
    user = current_usuario
    @zonas = user.empleado.zona
    @tipo_lecturas = TipoLectura.all
    @empleados = Empleado.joins(:usuario => [:perfil]).where(perfils:{id: [6..7]})
    @Claves = ClaveLectura.where(habilitada: true, efectivo:false).order(:nombre)
    @subempresa = Subempresa.all
  end

  # GET
  # Devuelve objeto con ordenes de lectura
  def cargar_ordenes
    # Rescato los valores desde la vista
    zona = params[:zona]
    proceso = params[:proceso]
    porcion = params[:porcion].to_i
    empleado = params[:empleado].to_i
    clave_lectura = params[:clave_lectura].to_i
    subempresa = params[:sub_empresa].to_i
    # Si porcion es igual 0
    # Busco todas las porciones por zona y abierto = true
    if porcion == 0
      porciones = Porcion.where(zona_id: zona, abierto: true)
      rutas = Rutum.where(porcion_id: porciones)
    else
      rutas = Rutum.where(porcion_id: porcion)
    end
    
    if empleado == 0
      empleado = Empleado.joins(:usuario).where(usuarios: {perfil_id: [6..7]})
    else
      empleado = Empleado.find(empleado)
    end
    if clave_lectura == 0
      clave_lectura = ClaveLectura.where(habilitada:true, efectivo:false)
    else
      clave_lectura = ClaveLectura.find(clave_lectura)
    end
    if subempresa == 0
      subempresa = Subempresa.all
    else
      subempresa = Subempresa.find(subempresa)
    end

    
    @ordenLecturas = OrdenLectura.where(rutum_id: rutas, estado_lectura_id: 4,tipo_lectura_id: proceso, subempresa_id: subempresa)
      .joins(:detalle_orden_lectura, :asignacion)
      .where(detalle_orden_lecturas: {clave_lectura_id: clave_lectura}, asignacions:{empleado_id: empleado})
      .group(:id)
      .order("detalle_orden_lecturas.fecha_ejecucion DESC")

    @ordenLecturas = @ordenLecturas.paginate(:page => params[:page], :per_page => 10)
    respond_with @ordenLecturas
  end

  # GET
  # Retorna Objeto @porciones
  def cargar_porciones
    p params[:zona]
    @porciones = Porcion.where(zona_id: params[:zona], abierto: true).order(:codigo)
    p @porciones
    respond_with @porciones
  end

  # GET
  # Devuelve hash con ubicación del recinto
  def ver_ubicacion_cliente
    ubicaciones = Array.new
    cliente = Cliente.find(params[:cliente])
    @cliente = cliente
    @clienteID = cliente.id
    if (!cliente.gps_latitud.nil? && !cliente.gps_longitud.nil?) && (cliente.gps_latitud.to_i != 0  && cliente.gps_longitud.to_i != 0)
      ventana = cliente.nombre.to_s + " | " + cliente.direccion
      ubicaciones.push([cliente.gps_latitud.to_s, cliente.gps_longitud.to_s, ventana])
      @latitud = cliente.gps_latitud.to_s
      @longitud = cliente.gps_longitud.to_s
    end
    @hash = Gmaps4rails.build_markers(ubicaciones) do |u, marker|
      marker.lat u[0]
      marker.lng u[1]
      marker.infowindow u[2]
    end
		respond_with @hash
  end

  def exportar_csv
    # Rescato los valores desde la vista
    zona = params[:zona]
    proceso = params[:proceso]
    porcion = params[:porcion].to_i
    empleado = params[:empleado].to_i
    clave_lectura = params[:clave_lectura].to_i
    subempresa = params[:sub_empresa].to_i
    # Si porcion es igual 0
    # Busco todas las porciones por zona y abierto = true
    if porcion == 0
      porciones = Porcion.where(zona_id: zona, abierto: true)
      rutas = Rutum.where(porcion_id: porciones)
    else
      rutas = Rutum.where(porcion_id: porcion)
    end
    
    if empleado == 0
      empleado = Empleado.joins(:usuario).where(usuarios: {perfil_id: [6..7]})
    else
      empleado = Empleado.find(empleado)
    end
    if clave_lectura == 0
      clave_lectura = ClaveLectura.where(habilitada:true, efectivo:false)
    else
      clave_lectura = ClaveLectura.find(clave_lectura)
    end
    if subempresa == 0
      subempresa = Subempresa.all
    else
      subempresa = Subempresa.find(subempresa)
    end
    
    @ordenLecturas = OrdenLectura.where(rutum_id: rutas, estado_lectura_id: 4,tipo_lectura_id: proceso, subempresa_id: subempresa)
      .joins(:detalle_orden_lectura, :asignacion)
      .where(detalle_orden_lecturas: {clave_lectura_id: clave_lectura}, asignacions:{empleado_id: empleado})
      .group(:id)
      .order("detalle_orden_lecturas.fecha_ejecucion DESC")

    header = "Codigo Orden,Nombre, Numero Cliente, Medidor,Ruta,Lect. Actual,Lect. Reactiva, Hora Punta Encontrada, Hora Punta Dejada, Hora, Fecha, Fuera Punta Encontrada, Fuera Punta Dejada, Reset, Nº Sello Encotrado, Nº Sello Dejado, Tarifa, Clave de Lectura,Condición de lectura,Observación de lectura,Comentario lector,Comentario recinto, Fecha, Latitud, Longitud, Observación Lector \n"
		fecha = Time.now.strftime("%d%m%Y%H%M%S").to_s
		file = "Visualizacion_lecturas - " +  fecha.to_s + ".csv"
		File.open(file, "w+:UTF-16LE:UTF-8") do |csv|
			csv << header
      @ordenLecturas.each do |orden|
        if !orden.detalle_orden_lectura.first.observacione.nil?
          observacion = orden.detalle_orden_lectura.first.observacione.descripcion
        else
          observacion = ""
        end
        # Tarifa BT1
        if orden.factor_cobro.nombre == "BT1"
        csv << orden.codigo.to_s + "," +
               orden.cliente.nombre + "," +
               orden.cliente.numero_cliente.to_s + "," +
               orden.medidor.numero_medidor.to_s + "," +
               orden.rutum.codigo.to_s + "," +
               orden.detalle_orden_lectura[0].lectura_actual.to_s + ",,,,,,,,,,," +
               orden.factor_cobro.nombre.to_s + "," +
               orden.detalle_orden_lectura.first.clave_lectura.nombre + "," +
               orden.detalle_orden_lectura.first.clave_lectura.descripcion_corta + "," +
               observacion + "," +
               orden.observacion + "," +
               orden.cliente.observacion_recinto.to_s + "," +
               orden.detalle_orden_lectura.first.fecha_ejecucion.strftime('%e/%m/%Y %I:%M:%S %p').to_s + "," +
               orden.gps_latitud.to_s + "," +
               orden.gps_longitud.to_s + "," +
               orden.observacion + "," + "\n"
        end
        # Tarifa BT2 && AT2
        if orden.factor_cobro.nombre == "BT2" || orden.factor_cobro.nombre == "AT2"
          csv << orden.codigo.to_s + "," +
                 orden.cliente.nombre + "," +
                 orden.cliente.numero_cliente.to_s + "," +
                 orden.medidor.numero_medidor.to_s + "," +
                 orden.rutum.codigo.to_s + "," +
                 orden.detalle_orden_lectura[0].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[1].lectura_actual.to_s + ",,,,,,,,,," +
                 orden.factor_cobro.nombre.to_s + "," +
                 orden.detalle_orden_lectura.first.clave_lectura.nombre + "," +
                 orden.detalle_orden_lectura.first.clave_lectura.descripcion_corta + "," +
                 observacion + "," +
                 orden.observacion + "," +
                 orden.cliente.observacion_recinto.to_s + "," +
                 orden.detalle_orden_lectura.first.fecha_ejecucion.strftime('%e/%m/%Y %I:%M:%S %p').to_s + "," +
                 orden.gps_latitud.to_s + "," +
                 orden.gps_longitud.to_s + "," +
                 orden.observacion + "," + "\n"
        end
        # Tarifa BT3 && AT3
        if orden.factor_cobro.nombre == "BT3" || orden.factor_cobro.nombre == "AT3"
          csv << orden.codigo.to_s + "," +
                 orden.cliente.nombre + "," +
                 orden.cliente.numero_cliente.to_s + "," +
                 orden.medidor.numero_medidor.to_s + "," +
                 orden.rutum.codigo.to_s + "," +
                 orden.detalle_orden_lectura[0].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[1].lectura_actual.to_s + ",,,,," +
                 orden.detalle_orden_lectura[2].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[3].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[4].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[5].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[6].lectura_actual.to_s + "," +
                 orden.factor_cobro.nombre.to_s + "," +
                 orden.detalle_orden_lectura.first.clave_lectura.nombre + "," +
                 orden.detalle_orden_lectura.first.clave_lectura.descripcion_corta + "," +
                 observacion + "," +
                 orden.observacion + "," +
                 orden.cliente.observacion_recinto.to_s + "," +
                 orden.detalle_orden_lectura.first.fecha_ejecucion.strftime('%e/%m/%Y %I:%M:%S %p').to_s + "," +
                 orden.gps_latitud.to_s + "," +
                 orden.gps_longitud.to_s + "," +
                 orden.observacion + "," + "\n"
        end
        # Tarifa BT43 && AT43
        if orden.factor_cobro.nombre == "BT43" || orden.factor_cobro.nombre == "AT43"
          csv << orden.codigo.to_s + "," +
                 orden.cliente.nombre + "," +
                 orden.cliente.numero_cliente.to_s + "," +
                 orden.medidor.numero_medidor.to_s + "," +
                 orden.rutum.codigo.to_s + "," +
                 orden.detalle_orden_lectura[0].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[1].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[2].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[3].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[7].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[6].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[4].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[5].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[8].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[9].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[10].lectura_actual.to_s + "," +
                 orden.factor_cobro.nombre.to_s + "," +
                 orden.detalle_orden_lectura.first.clave_lectura.nombre + "," +
                 orden.detalle_orden_lectura.first.clave_lectura.descripcion_corta + "," +
                 observacion + "," +
                 orden.observacion + "," +
                 orden.cliente.observacion_recinto.to_s + "," +
                 orden.detalle_orden_lectura.first.fecha_ejecucion.strftime('%e/%m/%Y %I:%M:%S %p').to_s + "," +
                 orden.gps_latitud.to_s + "," +
                 orden.gps_longitud.to_s + "," +
                 orden.observacion + "," + "\n"
        end
			end
		end
		send_file(file, x_sendfile: true, buffer_size: 512, disposition: 'attachment')
  end

end
