class ReporteControlHorasTerrenoController < ApplicationController
	respond_to :js, :html, :json

	def index
		user = current_usuario
    @zonas = user.empleado.zona
		@tipo_lectura = TipoLectura.all
		@empleados = Empleado.joins(:usuario, :asignacion => [:porcion]).where(usuarios: {perfil_id: [6,7], porcions: {abierto: true}}).group(:id).order(:nombre)
	end
	# => Carga Porciones
	def carga_porciones
    @porciones = Porcion.where(zona_id: params[:zona], abierto: true).joins(:asignacion => [:porcion]).where(porcions:{abierto:true}).group(:id).order(:codigo)
		respond_with @porciones
	end

	# => Carga Comunas
	def carga_comunas
		@comunas = Comuna.where(zona_id: params[:zona])
		respond_with @comunas
	end
	# > Metodo encargado de listar usarios devuelve objeto de usuarios
	def carga_empleados
		# > Busca los empleados  por zona y perfil 6, 7
    porcion = Porcion.where(id: params[:porcion]).take
		if !porcion.nil?
			@empleados = Empleado.joins(:usuario).where(usuarios: {perfil_id: [6,7]})
		end
    respond_with @empleados
	end


	def carga_filtro
		totalHoras = 0
		totalMinutos = 0
		totalSegundos = 0
		porciones = Porcion.where(zona_id: params[:zona],abierto: true)
    rutas = Rutum.where(porcion_id: params[:porcion])
    if rutas.blank?
      rutas = Rutum.joins(:porcion).where(porcions: {zona_id: params[:zona], abierto:true})
		end
		empleados = Empleado.where(id: params[:empleado].to_i).joins(:usuario, :asignacion).where(asignacions: {porcion_id: porciones}).group(:id)
		if empleados.blank?
			empleados = Empleado.joins(:usuario, :asignacion).where(usuarios: {perfil_id: [6,7]}, asignacions: {porcion_id: porciones}).group(:id)
		end
   	lista = Array.new
		empleados.each do |empleado|
      asignaciones = Asignacion.where(empleado_id: empleado.id).joins(:porcion, :orden_lectura).where(porcions:{ id: porciones },orden_lecturas:{estado_lectura_id: [2..5], rutum_id: rutas, relectura: false})
			asignacionesRutas = asignaciones.group(:rutum_id)
			p asignacionesRutas
			asignacionesRutas.each do |asignacion|
				ruta = asignacion.rutum
				asignado = Asignacion.where(empleado_id: empleado.id).joins(:porcion, :orden_lectura).where(porcions:{ id: porciones },orden_lecturas:{estado_lectura_id: [2..5], rutum_id: ruta.id, relectura: false}).count
        pendiente = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [3], relectura: false}).count
				parsed_date= Date.parse(params[:fecha])
				hora = OrdenLectura.where(rutum_id: asignacion.rutum_id, estado_lectura_id: [4,5], relectura:false).joins(:asignacion, :detalle_orden_lectura).where(asignacions: {empleado_id: asignacion.empleado_id, detalle_orden_lecturas: {fecha_ejecucion: parsed_date.midnight..parsed_date.end_of_day}}).order("detalle_orden_lecturas.fecha_ejecucion ASC")
        if !hora.blank? then
          hora_inicio = hora.first.detalle_orden_lectura.first.fecha_ejecucion
					hora_termino = hora.last.detalle_orden_lectura.first.fecha_ejecucion
					as = TimeDifference.between(hora_termino , hora_inicio).in_seconds
					total = hora_termino - hora_inicio
					mm, ss = total.divmod(60)
					hh, mm = mm.divmod(60)
					if hh == 0
						hh = "00"
					end
					if mm == 0
						mm = "00"
					end
				else
					ss = 00
					mm = 00
					hh= 00
          hora_inicio = ""
          hora_termino = ""
				end
				if (hora_inicio == "" ) || (hora_termino == "" )
					lista.push([ruta.porcion.codigo,empleado.nombre_completo, ruta.codigo,asignado,pendiente, hora_inicio, hora_termino, "0"])
				else
					lista.push([ruta.porcion.codigo,empleado.nombre_completo, ruta.codigo,asignado,pendiente, hora_inicio.strftime("%H:%M:%S %d/%m/%Y"), hora_termino.strftime("%H:%M:%S %d/%m/%Y"), "%s:%s:%d" % [hh, mm, ss]])
				end
			end
			total = 0, hora_inicio = 0, hora_termino = 0 
		end
		#> Ordena Lista por Nombre Ruta
		lista.sort! {|a,b| a[0] <=> b[0]} 
		@lista = lista.paginate(:page => params[:page], :per_page => 10)
 		respond_with @lista
	end
# Exportar Csv
def exportar_csv
	totalHoras = 0
	totalMinutos = 0
	totalSegundos = 0
	porciones = Porcion.where(zona_id: params[:zona],abierto: true)
	rutas = Rutum.where(porcion_id: params[:porcion])
	if rutas.blank?
		rutas = Rutum.joins(:porcion).where(porcions: {zona_id: params[:zona], abierto:true})
	end
	empleados = Empleado.where(id: params[:empleado].to_i).joins(:usuario, :asignacion).where(asignacions: {porcion_id: porciones}).group(:id)
	if empleados.blank?
		empleados = Empleado.joins(:usuario, :asignacion).where(usuarios: {perfil_id: [6,7]}, asignacions: {porcion_id: porciones}).group(:id)
	end
	 lista = Array.new
	empleados.each do |empleado|
		asignaciones = Asignacion.where(empleado_id: empleado.id).joins(:porcion, :orden_lectura).where(porcions:{ id: porciones },orden_lecturas:{estado_lectura_id: [2..5], rutum_id: rutas, relectura: false})
		asignacionesRutas = asignaciones.group(:rutum_id)
		p asignacionesRutas
		asignacionesRutas.each do |asignacion|
			ruta = asignacion.rutum
			asignado = Asignacion.where(empleado_id: empleado.id).joins(:porcion, :orden_lectura).where(porcions:{ id: porciones },orden_lecturas:{estado_lectura_id: [2..5], rutum_id: ruta.id, relectura: false}).count
			pendiente = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas: {estado_lectura_id: [3], relectura: false}).count
			parsed_date= Date.parse(params[:fecha])
			hora = OrdenLectura.where(rutum_id: asignacion.rutum_id, estado_lectura_id: [4,5], relectura:false).joins(:asignacion, :detalle_orden_lectura).where(asignacions: {empleado_id: asignacion.empleado_id, detalle_orden_lecturas: {fecha_ejecucion: parsed_date.midnight..parsed_date.end_of_day}}).order("detalle_orden_lecturas.fecha_ejecucion ASC")
			if !hora.blank? then
				hora_inicio = hora.first.detalle_orden_lectura.first.fecha_ejecucion
				hora_termino = hora.last.detalle_orden_lectura.first.fecha_ejecucion
				as = TimeDifference.between(hora_termino , hora_inicio).in_seconds
				total = hora_termino - hora_inicio
				mm, ss = total.divmod(60)
				hh, mm = mm.divmod(60)
				if hh == 0
					hh = "00"
				end
				if mm == 0
					mm = "00"
				end
			else
				ss = 00
				mm = 00
				hh= 00
				hora_inicio = ""
				hora_termino = ""
			end
			if (hora_inicio == "" ) || (hora_termino == "" )
				lista.push([ruta.porcion.codigo,empleado.nombre_completo, ruta.codigo,asignado,pendiente, hora_inicio, hora_termino, "0"])
			else
				lista.push([ruta.porcion.codigo,empleado.nombre_completo, ruta.codigo,asignado,pendiente, hora_inicio.strftime("%H:%M:%S %d/%m/%Y"), hora_termino.strftime("%H:%M:%S %d/%m/%Y"), "%s:%s:%d" % [hh, mm, ss]])
			end
		end
		total = 0, hora_inicio = 0, hora_termino = 0 
	end
	#> Ordena Lista por Nombre Ruta
	lista.sort! {|a,b| a[0] <=> b[0]} 
	
		header = "Ciclo, Ruta, Lector, Clientes Asignados,Clientes Pendientes,Hora Comienzo, Hora Termino, Total Horas\n"
		fecha = Time.now
		file = "control_horas - " +  fecha.to_s + ".csv"
		fecha_busqueda = params[:fecha]
		File.open(file, "w+:UTF-16LE:UTF-8") do |csv|
			csv << "Fecha Busqueda: " + fecha_busqueda + "\n" 
			csv << header
				lista.each do |l|
					csv << l[0].to_s + "," +
								 l[2].to_s + "," + 
								 l[1].to_s + "," +
								 l[3].to_s + "," +
								 l[4].to_s + "," +
								 l[5].to_s + "," +
								 l[6].to_s + "," +
								 l[7].to_s + "," + "\n"
				end
		end
		send_file(file, x_sendfile: true, buffer_size: 512)
end


end	
