
class LecturaDictadaClienteController < ApplicationController
respond_to :js, :json, :html

def index

end

def form_lectura_dictada
  seleccion = params[:seleccion].to_i
  seleccion = params[:seleccion].to_i
  if seleccion == 1 #Busca Por Cliente
    @lecturas= OrdenLectura.joins(:cliente).where('clientes.numero_cliente = ?', params[:busqueda].to_i)
  elsif seleccion == 2
    @lecturas = OrdenLectura.joins(:medidor).where('medidors.numero_medidor = ? ', params[:busqueda].to_i)
  end
  @cliente = ordenesLectura.first.cliente
  @ordenLectura = ordenesLectura.take
end

def tabla_contenido
  @listaOrdenes = Array.new
  seleccion = params[:seleccion].to_i  
  p seleccion
  #Busca Por Cliente
  if seleccion == 1 
    ordenesLectura = OrdenLectura.joins(:cliente).where(clientes:{numero_cliente: params[:busqueda].to_i})
  #Busca por Numero Medidor
  elsif seleccion == 2
    ordenesLectura = OrdenLectura.joins(:detalle_orden_lectura => [:medidor]).where(medidors:{numero_medidor: params[:busqueda]})
  end
  @cliente = ordenesLectura.take.cliente
  @ordenLectura = ordenesLectura.take
  ordenesLectura.each do |orden|
    fila = Array.new
    fila.push(orden.id)
    fila.push(orden.comuna.nombre.upcase)
    fila.push(orden.tipo_lectura.nombre)
    fila.push(orden.rutum.porcion.mes.to_s + "/" + orden.rutum.porcion.ano.to_s)
    if !orden.fecha_asignacion.blank?  
      fila.push(orden.fecha_asignacion.strftime("%d/%m/%Y %H:%M:%S").to_s)
    else 
      fila.push("")
    end 
    if !orden.detalle_orden_lectura.take.nil?
      if !orden.detalle_orden_lectura.take.fecha_ejecucion.blank?  
        fila.push(orden.detalle_orden_lectura.take.fecha_ejecucion.strftime("%d/%m/%Y %H:%M:%S").to_s)
      else 
        fila.push("No registra")
      end 
    end
    if !orden.asignacion.nil?  
      fila.push(orden.asignacion.empleado.nombre_completo.upcase)
    else
      fila.push("No registra")
    end
    fila.push(orden.estado_lectura.nombre)
    if !orden.detalle_orden_lectura.first.clave_lectura.nil?
      fila.push(orden.detalle_orden_lectura.first.clave_lectura.nombre)
    else 
      fila.push("No registra")
    end    
    if !orden.detalle_orden_lectura.take.nil?
      claveAnterior = orden.detalle_orden_lectura.first.clave_lectura_anterior
      fila.push(claveAnterior)
    else
      fila.push("No registra")
    end
    fila.push(orden.detalle_orden_lectura.first.lectura_actual)
    fila.push(orden.lectura_dictada)    

    @listaOrdenes.push(fila)
  end
  respond_with @listaOrdenes
end


def agregar_lectura_dictada
  ordenLectura = OrdenLectura.find(params[:orden_id])
  ordenLectura.update(lectura_dictada: params[:lectura_dictada])
  @completado = Array.new
  @completado.push("Asignacion Creada Correctamente",params[:lectura_dictada])
  render 'alertas'
end

end
