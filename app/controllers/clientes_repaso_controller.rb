class ClientesRepasoController < ApplicationController
	respond_to :js, :html, :json
	
	def index
		user = current_usuario
    @zonas = user.empleado.zona
	end

  def carga_filtro
    porciones = Porcion.where(zona_id: params[:zona])
    if params[:seleccion].to_i == 1
		  @ordenes = Asignacion.where(porcion_id: porciones).joins(:orden_lectura).where(orden_lecturas:{tipo_lectura_id: [2], estado_lectura_id: [2..3]}).paginate(:page => params[:page], :per_page => 10)
    end
    if params[:seleccion].to_i == 2
      @ordenes = Asignacion.where(porcion_id: porciones).joins(:orden_lectura).where(orden_lecturas:{tipo_lectura_id: [2], estado_lectura_id: [2..3], analisis: 0}).paginate(:page => params[:page], :per_page => 10)
    end
    if params[:seleccion].to_i == 3
      @ordenes = Asignacion.where(porcion_id: porciones).joins(:orden_lectura).where(orden_lecturas:{tipo_lectura_id: [2], estado_lectura_id: [2..3], analisis: 1}).paginate(:page => params[:page], :per_page => 10)
    end

  end
end
