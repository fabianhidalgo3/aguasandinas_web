class ReporteEfectividadLectorController < ApplicationController
	respond_to :js, :html, :json, :pdf

	def index # > Pagina Principal
		user = current_usuario	# > Busca usuario y zonas y porcion del usuario logueado
    @zonas = user.empleado.zona
		@totales = Array.new
	end

	def carga_lectores 	# => Carga Porciones		
		zona = params[:zona].to_i
		@lectores = Empleado.joins(:usuario, :zona).where(usuarios:{perfil_id: [6,7]}, zonas:{id: zona})
		respond_with @lectores
	end

	def carga_filtro 	# => Filtro del Reporte		
		@lista = Array.new
		@totales = Array.new
		t_asignado = 0
		t_claveEfectivo = 0
		t_claveImprocedente = 0
		t_problemasTecnicos = 0
		asignado = 0
		claveEfectivo = 0
		claveImprocedente = 0
		problemasTecnicos = 0
		detalles = 0
		tDetalles = 0
		total = 0
		t_efectividad =0
		contador = 0
		efectividad = 0
		if params[:zona] != "" && params[:lector] == ""
			listaEmpleados = Empleado.joins(:usuario).where(usuarios: {perfil_id: [6,7]})		
		end
		if params[:zona] != "" && params[:lector] != ""		
			listaEmpleados = Empleado.joins(:usuario).where(usuarios: {id: params[:lector].to_i})		
		end
	 	listaEmpleados.each do |empleado|
			listaAsignaciones = Asignacion.where(empleado_id: empleado.id).joins(:porcion,
				:orden_lectura).where(porcions:{ zona_id: params[:zona].to_i},
				orden_lecturas:{estado_lectura_id: [2,3,4,5]}).group(:rutum_id)
		
		 if !listaAsignaciones.blank?
			 listaAsignaciones.each do |asignacion|
				 ruta = asignacion.rutum
				 comuna = asignacion.orden_lectura.comuna.nombre
				 asignado = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas: {relectura:false}).count
				 detalles = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5], relectura:false}).count
				 claveEfectivo = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5], relectura:false}, clave_lecturas: {efectivo: true}).count
				 claveImprocedente = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5], relectura:false}, clave_lecturas: {efectivo: false}).count
				 problemasTecnicos = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5], relectura:false}, clave_lecturas: {id: [6,7,11,12,14,19,21,22,23]}).count
				 efectividad = ((claveEfectivo*100.0)/asignado).round(2)
				 @lista.push([ruta.porcion.codigo, comuna, empleado.nombre_completo,ruta.codigo, asignado, claveEfectivo, claveImprocedente, problemasTecnicos, (claveEfectivo + claveImprocedente),efectividad, detalles])
			 
				contador = contador + 1
				t_asignado = t_asignado + asignado	
				t_claveEfectivo = t_claveEfectivo + claveEfectivo
				t_claveImprocedente = t_claveImprocedente + claveImprocedente
				t_problemasTecnicos = t_problemasTecnicos + problemasTecnicos	
				total = (t_claveImprocedente + t_claveEfectivo)
				t_efectividad = t_efectividad + efectividad
				tDetalles = tDetalles + detalles
			end 	
		 end 
	 end
	 @totales.push(t_asignado, t_claveEfectivo, t_claveImprocedente, t_problemasTecnicos, total, (t_efectividad/contador).round(2), tDetalles)
	 # > Ordena Lista por Nombre Ruta
	 @lista.sort! {|a,b| a[3] <=> b[3]}
	 respond_with @lista
	end

	# > Exportar Documento CSV
	def exportar_csv 
		lector = params[:lector]
		zona = params[:zona]
		@lista = Array.new
		@totales = Array.new
		t_asignado = 0
		t_claveEfectivo = 0
		t_claveImprocedente = 0
		t_problemasTecnicos = 0
		asignado = 0
		claveEfectivo = 0
		claveImprocedente = 0
		problemasTecnicos = 0
		detalles = 0
		tDetalles = 0
		total = 0
		t_efectividad =0
		contador = 0
		efectividad = 0
		if zona != "" && lector == ""
			listaEmpleados = Empleado.joins(:usuario).where(usuarios: {perfil_id: [6,7]})		
		end
		if zona != "" && lector != ""		
			listaEmpleados = Empleado.joins(:usuario).where(usuarios: {id: lector.to_i})		
    end
	 	listaEmpleados.each do |empleado|
			listaAsignaciones = Asignacion.where(empleado_id: empleado.id).joins(:porcion,:orden_lectura).where(porcions:{ zona_id: zona.to_i},orden_lecturas:{estado_lectura_id: [2,3,4,5]}).group(:rutum_id)
      p listaAsignaciones
      if !listaAsignaciones.blank?
			 listaAsignaciones.each do |asignacion|
				 ruta = asignacion.rutum
				 comuna = asignacion.orden_lectura.comuna.nombre
				 asignado = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas: {relectura:false}).count
				 detalles = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5], relectura:false}).count
				 claveEfectivo = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5], relectura:false}, clave_lecturas: {efectivo: true}).count
				 claveImprocedente = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5], relectura:false}, clave_lecturas: {efectivo: false}).count
				 problemasTecnicos = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5], relectura:false}, clave_lecturas: {id: [6,7,11,12,14,19,21,22,23]}).count
				 efectividad = ((claveEfectivo*100.0)/asignado).round(2)
				 @lista.push([ruta.porcion.codigo_nombre, comuna, empleado.nombre_completo,ruta.codigo, asignado, claveEfectivo, claveImprocedente, problemasTecnicos, (claveEfectivo + claveImprocedente),efectividad, detalles])
			 
				contador = contador + 1
				t_asignado = t_asignado + asignado	
				t_claveEfectivo = t_claveEfectivo + claveEfectivo
				t_claveImprocedente = t_claveImprocedente + claveImprocedente
				t_problemasTecnicos = t_problemasTecnicos + problemasTecnicos	
				total = (t_claveImprocedente + t_claveEfectivo)
				t_efectividad = t_efectividad + efectividad
				tDetalles = tDetalles + detalles
			end 	
		 end 
	 end
	 @totales.push(t_asignado, t_claveEfectivo, t_claveImprocedente, t_problemasTecnicos, total,(t_efectividad/contador).round(2), tDetalles)
	 # > Ordena Lista por Nombre Ruta
	 @lista.sort! {|a,b| a[3] <=> b[3]}
		# > Ordena Lista por Nombre Ruta
	 	@lista.sort! {|a,b| a[3] <=> b[3]}
    header = "Ciclo, Comuna, Lector, Ruta,Medidores Asignados, Lecturas Asignadas,Claves Efectivas,Claves Improcedentes,Problemas Tecnicos,Total, % Efectividad\n"
		fecha = Time.now.strftime("%d%m%Y%H%M%S").to_s
		file = "reporte_efectividad_lector - " +  fecha.to_s + ".csv"
		File.open(file, "w+:UTF-16LE:UTF-8") do |csv|
			csv << header
			@lista.each do |l|
				csv << l[0].to_s + "," + l[1].to_s + "," + l[2].to_s + "," + 
							 l[3].to_s + "," + l[4].to_s + "," + l[10].to_s + "," + l[5].to_s + "," + 
							 l[6].to_s + "," + l[7].to_s + "," + l[8].to_s + "," + 
							 l[9].to_s + "," + "\n"
			end
			csv << "" + "," + "" + "," + "" + "," + "" + "," +
						 @totales[0].to_s + "," + @totales[6].to_s + "," + @totales[1].to_s + "," +
						 @totales[2].to_s + "," + @totales[3].to_s + "," +
						 @totales[4].to_s + "," + @totales[5].to_s + "," + "\n"
		end
		send_file(file, x_sendfile: true, buffer_size: 512, disposition: 'attachment') 
	end


	def exportar_pdf
		# Información del Archivo
    fechaDescarga = Time.now.strftime("%d-%m-%Y_%H%M%S").to_s
		nombreArchivo = "efectividad_lector_" +  fechaDescarga.to_s + ".pdf"
		# Se Crea lista
		lector = params[:lector]
		zona = params[:zona]
		@lista = Array.new
		@totales = Array.new
		t_asignado = 0
		t_claveEfectivo = 0
		t_claveImprocedente = 0
		t_problemasTecnicos = 0
		asignado = 0
		claveEfectivo = 0
		claveImprocedente = 0
		problemasTecnicos = 0
		detalles = 0
		tDetalles = 0
		total = 0
		t_efectividad =0
		contador = 0
		efectividad = 0
		if zona != "" && lector == ""
			listaEmpleados = Empleado.joins(:usuario).where(usuarios: {perfil_id: [6,7]})		
		end
		if zona != "" && lector != ""		
			listaEmpleados = Empleado.joins(:usuario).where(usuarios: {id: lector.to_i})		
    end
	 	listaEmpleados.each do |empleado|
			listaAsignaciones = Asignacion.where(empleado_id: empleado.id).joins(:porcion,:orden_lectura).where(porcions:{ zona_id: zona.to_i},orden_lecturas:{estado_lectura_id: [2,3,4,5]}).group(:rutum_id)

			if !listaAsignaciones.blank?
				listaAsignaciones.each do |asignacion|
				 	ruta = asignacion.rutum
					comuna = asignacion.orden_lectura.comuna.nombre
				 	asignado = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura).where(orden_lecturas: {relectura:false}).count
				 	detalles = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5], relectura:false}).count
				 	claveEfectivo = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5], relectura:false}, clave_lecturas: {efectivo: true}).count
				 	claveImprocedente = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5], relectura:false}, clave_lecturas: {efectivo: false}).count
				 	problemasTecnicos = Asignacion.where(empleado_id: asignacion.empleado_id, rutum_id: asignacion.rutum_id).joins(:orden_lectura => [:detalle_orden_lectura => :clave_lectura]).where(orden_lecturas: {estado_lectura_id: [4,5], relectura:false}, clave_lecturas: {id: [6,7,11,12,14,19,21,22,23]}).count
				 	efectividad = ((claveEfectivo*100.0)/asignado).round(2)
				 	@lista.push([ruta.porcion.codigo_nombre, comuna, empleado.nombre_completo,ruta.codigo, asignado,detalles, claveEfectivo, claveImprocedente, problemasTecnicos, (claveEfectivo + claveImprocedente),efectividad])
				
					contador = contador + 1
					t_asignado = t_asignado + asignado	
					t_claveEfectivo = t_claveEfectivo + claveEfectivo
					t_claveImprocedente = t_claveImprocedente + claveImprocedente
					t_problemasTecnicos = t_problemasTecnicos + problemasTecnicos	
					total = (t_claveImprocedente + t_claveEfectivo)
					t_efectividad = t_efectividad + efectividad
					tDetalles = tDetalles + detalles
				end 	
		 	end 
	 	end
	 	@totales.push("","","","Totales:  ",t_asignado,tDetalles, t_claveEfectivo, t_claveImprocedente, t_problemasTecnicos, total,(t_efectividad/contador).round(2))
		@lista.push(@totales)
		 respond_to do |format|
			format.pdf do
        pdf = CicloLectorPdf.new(@lista, @totales)
        send_data pdf.render, filename: nombreArchivo, type: 'application/pdf'
			end
		end
  end

end
