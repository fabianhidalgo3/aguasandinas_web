class ClaveLecturasController < ApplicationController
  
  def index
		@clave_lecturas = ClaveLectura.all.order(:id)
		@totalClaves = ClaveLectura.all.count
		@totalObservaciones = Observacione.all.count
  end

  # POST
  # Create
  def new
    @clave_lecturas = ClaveLectura.new
  end

  # GET
  # Importar Claves de lectura CSV
  def import
    require 'csv'
    contadorClavesImportadas = 0
    contadorClavesNoImportadas = 0
    totalClaves = 0
    CSV.foreach(params[:file].path, headers: true) do |row|
      @clave_lectura = ClaveLectura.where(id: row[0]).first
      if @clave_lectura.nil?
        @claveLectura = ClaveLectura.create(id: row[0], codigo: row[1], nombre: row[2], efectivo: row[3], requerido: row[4], verificacion: row[5], num_fotografias: row[6], foto: row[7]).save
        contadorClavesImportadas = contadorClavesImportadas +1
      else
        contadorClavesNoImportadas = contadorClavesNoImportadas + 1
      end
      totalClaves = totalClaves + 1
    end
    if contadorClavesImportadas != 0
      flash[:success] = "Se cargaron #{contadorClavesImportadas} / #{totalClaves}  Claves de lectura"
    end
    if contadorClavesNoImportadas != 0
      flash[:danger] =  "No se pueden importar #{contadorClavesNoImportadas} / #{totalClaves} Claves de lectura, ya se encuentran registradas en sistema"
    end
    redirect_to clave_lecturas_path
  end

  # POST
  # Importar Claves de lectura CSV
  def form_import

  end

  # > Get 
  # > Exportar CSV
  def exportar_csv
    listaClavelecturas = Array.new
    claveLecturas = Observacione.all
    claveLecturas.each do |c|
      if (c.requerido)
        requerido =  "Si"
      else
        requerido ="No"
			end
      if (c.efectivo)
        efectivo =  "Si"
      else
        efectivo ="No"
      end
      listaClavelecturas.push([c.clave_lectura.nombre, c.codigo_ptc, c.codigo, c.descripcion, requerido,efectivo])
    end    
    cabecera = "Clave Lectura, Código PTC, Código, Descripción, Lectura Requerida, Lectura Efectiva\n"
    fechaDescarga = Time.now.strftime("%d-%m-%Y_%H%M%S").to_s
    nombreArchivo = "claves_lecturas_ "+fechaDescarga+".csv"
    File.open(nombreArchivo, "w+:UTF-16LE:UTF-8") do |fila|
      fila << cabecera
      listaClavelecturas.each do |l|
        fila << l[0].to_s + "," + l[1].to_s + "," + l[2].to_s + "," + l[3].to_s + "," + l[4].to_s + "," + l[5].to_s + "\n"
      end
    end
    send_file(nombreArchivo, x_sendfile: true, buffer_size: 512)
  end

  # > GET
  # > Exportar PDF
  def exportar_pdf
  	# Informacion del Archivo
    fechaDescarga = Time.now.strftime("%d-%m-%Y_%H%M%S").to_s
    nombreArchivo = "clave_lecturas_ "+fechaDescarga+".pdf"
  	listaClaves = Observacione.all
		respond_to do |format|
			format.pdf do
        pdf = ClaveLecturasPdf.new(listaClaves)  
        send_data pdf.render, filename: nombreArchivo, type: 'application/pdf'
			end
		end
  end

end
