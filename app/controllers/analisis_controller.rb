class AnalisisController < ApplicationController
  respond_to :js, :html, :json

  # => Modulo Clientes dudosos
  def dudosos
    user = current_usuario
    @zonas = user.empleado.zona
    @tipo_lecturas = TipoLectura.all
  end

  # => Modulo Clientes improcedentes
  def improcedentes
    user = current_usuario
    @zonas = user.empleado.zona
    @tipo_lecturas = TipoLectura.all
    user = current_usuario
    zonas = user.empleado.zona
    @regiones = Region.joins(:zona).where(zonas:{id: zonas}).group(:id)
  end


  # => autoriza_Facturacion
  def autoriza_facturacion
    @orden = OrdenLectura.where(id: params[:orden_id]).first
    @orden.autorizado_facturacion = true
    @orden.save
    @completado = [true, params[:analisis].to_i]
    respond_with @completado
  end

  # => Releer
  def releer
    @analisis = params[:analisis]
    @orden = OrdenLectura.where(id: params[:orden]).first
    @detalle = DetalleOrdenLectura.where(orden_lectura_id: @orden.id).first
    zona = @orden.rutum.porcion.zona
    @lectores = zona.empleado.joins(:usuario).where(usuarios: {perfil_id: [6..7]})
    respond_with @orden
  end

  # => Carga la fotografia de los analisis dudosos
  def carga_fotografia
    detalle = DetalleOrdenLectura.where(orden_lectura_id: params[:id_orden]).first
    @numero_cliente = detalle.orden_lectura.cliente.numero_cliente
    @lista_fotos = Fotografium.joins(:detalle_orden_lectura => [:orden_lectura]).where(orden_lecturas: {id:params[:id_orden]})
    respond_with @lista_fotos
  end


  # GET
  # Retorna lista con zonas
  def carga_zonas
    user = current_usuario
    empleado = user.empleado
    @zonas = Zona.where(region_id: params[:region]).joins(:empleado).where(empleados: {id: empleado.id})
    respond_with @zonas
  end

  # GET
  # Retorna lista con porciones
  def carga_porciones
    @porciones = Porcion.where(zona_id: params[:zona], abierto: true).order(:codigo)
    respond_with @porciones
  end

  # GET
  # Retorna lista con rutas
  def carga_rutas
    @rutas = Rutum.where(porcion_id: params[:porcion])
    respond_with @rutas
  end

  # GET
  # Retorna lista con empleados
  def carga_lectores
    @lectores = Empleado.joins(:usuario, :zona).where(usuarios:{perfil_id: [6,7]}, zonas:{id: params[:zona]})
    respond_with @lectores
  end

	# => Carga Comunas por Región y Zona
	def carga_comunas
		user = current_usuario
    zonas = user.empleado.zona
		@comunas = Comuna.where(zona_id: 1)
    respond_with @comunas
	end

	# => Lista de datos Carga Dudosos maximos y minimos.
	def carga_dudosos
    zona = params[:zona]
    proceso = params[:proceso]
    porcion = params[:porcion].to_i
    ruta = params[:ruta]
    lector = params[:lector]
    porciones = Porcion.where(zona_id: zona, abierto: true)
    if porcion == 0
      rutas = Rutum.where(porcion_id: porciones)
    else
      rutas = Rutum.where(porcion_id: porcion)
    end
    if !rutas.nil?
      @ordenLecturas = OrdenLectura.where(rutum_id: rutas, estado_lectura_id: 4,tipo_lectura_id: proceso).joins(:detalle_orden_lectura).group(:id).order("detalle_orden_lecturas.fecha_ejecucion DESC")
    end
    
    @ordenLecturas = @ordenLecturas.paginate(:page => params[:page], :per_page => 2)
    respond_with @ordenLecturas
	end

  # => Lista de datos. improcedentes
	def carga_improcedentes
    zona = params[:zona]
    proceso = params[:proceso]
    porcion = params[:porcion].to_i
    porciones = Porcion.where(zona_id: zona, abierto: true)
    if porcion == 0
      rutas = Rutum.where(porcion_id: porciones)
    else
      rutas = Rutum.where(porcion_id: porcion)
    end
    if !rutas.nil?
      @ordenLecturas = OrdenLectura.where(rutum_id: rutas, estado_lectura_id: 4,tipo_lectura_id: proceso).joins(:detalle_orden_lectura => [:clave_lectura]).where(clave_lecturas: {efectivo:false}).group(:id).order("detalle_orden_lecturas.fecha_ejecucion DESC")
    end
    @ordenLecturas = @ordenLecturas.paginate(:page => params[:page], :per_page => 10)
    respond_with @ordenLecturas
	end

  # > Get
  # > @orden_lectura = Objeto Detalle
  # > Busca el detalle de la orden
  def modificar_lectura
    @orden_lectura = DetalleOrdenLectura.where(orden_lectura_id: params[:id_orden].to_i).first
    p @orden_lectura
  end

  # > Guardo lectura Actual Modificada
  def guardar_lectura
    orden = DetalleOrdenLectura.where(id: params[:id].to_i).first
    orden.update(lectura_actual: params[:lectura].to_i)
    orden.save
    #orden.detalle_orden_lectura.lectura_actual = params[:lectura].to_id
  end

   # => Guarda Nueva Lectura con estado Relectura y la Asigna a un Lector
   def relectura
    @completado = Array.new
    # > Busco la orden
		ordenLectura = OrdenLectura.where(id: params[:orden_id]).first

    # > Creo la nueva orden lectura en estado de Verificación
    nuevaOrdenLectura = OrdenLectura.create(
      :codigo => ordenLectura.codigo,
      :posicion => ordenLectura.posicion,
      :secuencia_lector => ordenLectura.posicion,
      :direccion => ordenLectura.direccion,
      :direccion_entrega => ordenLectura.direccion_entrega,
      :numero_poste => ordenLectura.numero_poste,
      :fecha_carga => ordenLectura.fecha_carga,
      :fecha_propuesta => ordenLectura.fecha_propuesta,
      :fecha_asignacion => Time.zone.now,
      :instalacion_id => ordenLectura.instalacion_id,
      :factor_cobro_id => ordenLectura.factor_cobro.id,
      :cliente_id => ordenLectura.cliente_id,
      :rutum_id => ordenLectura.rutum_id,
      :estado_lectura_id => 2,
      :tipo_lectura_id => 2,
      :medidor_id => ordenLectura.medidor_id,
      :tipo_establecimiento_id => 1,
      :nro_municipal => ordenLectura.nro_municipal,
      :comuna_id => ordenLectura.comuna_id,
      :relectura => false,
      :analisis => params[:analisis].to_i,
      :color => ordenLectura.color,
      :num_digitos => ordenLectura.num_digitos
    )
    nuevaOrdenLectura.save

    # Busco los detalles a actualizar
    detalles = DetalleOrdenLectura.where(orden_lectura_id: params[:orden_id])

    # Crea un nuevo detalle
    if detalles.count == 1
      nuevoDetalleOrdenLectura = DetalleOrdenLectura.create(
        :orden_lectura_id => nuevaOrdenLectura.id,
        :numerador_id => ordenLectura.detalle_orden_lectura.first.numerador_id,
        :lectura_anterior => ordenLectura.detalle_orden_lectura.first.lectura_anterior,
        :lectura_promedio => ordenLectura.detalle_orden_lectura.first.lectura_promedio,
        :clave_lectura_anterior => ordenLectura.detalle_orden_lectura.first.clave_lectura_anterior,
        :tipo_consumo_id => ordenLectura.detalle_orden_lectura.first.tipo_consumo_id)
      nuevoDetalleOrdenLectura.save
    end

    # > Recorre los detalles y los crea nuevamente
    if detalles.count > 1
      detalles.each do |detalle|
        nuevoDetalleOrdenLectura = DetalleOrdenLectura.create(
          :orden_lectura_id => nuevaOrdenLectura.id,
          :numerador_id => detalle.numerador_id,
          :lectura_anterior => detalle.lectura_anterior,
          :lectura_promedio => detalle.lectura_promedio,
          :clave_lectura_anterior => detalle.clave_lectura_anterior,
          :tipo_consumo_id => detalle.tipo_consumo_id
        )
        nuevoDetalleOrdenLectura.save
      end
    end

    # > Asigna la Verificación a un Empleado
    nuevaAsignacion = Asignacion.create(
      :rutum_id => ordenLectura.rutum_id,
      :porcion_id => ordenLectura.rutum.porcion.id,
      :orden_lectura_id  => nuevaOrdenLectura.id,
      :empleado_id => params[:lector].to_i
    )
		nuevaAsignacion.save

    ordenLectura.update(:relectura => true)
    ordenLectura.save
    @completado.push([1, params[:analisis].to_i])
    respond_with @completado
  end

  def exportar_lecturas_csv
    zona = params[:zona]
    proceso = params[:proceso]
    porcion = params[:porcion].to_i
    porciones = Porcion.where(zona_id: zona, abierto: true)
    if porcion == 0
      rutas = Rutum.where(porcion_id: porciones)
    else
      rutas = Rutum.where(porcion_id: porcion)
    end
    if !rutas.nil?
      @ordenLecturas = OrdenLectura.where(rutum_id: rutas, estado_lectura_id: 4,tipo_lectura_id: proceso).joins(:detalle_orden_lectura).group(:id).order("detalle_orden_lecturas.fecha_ejecucion DESC")
    end

    header = "Codigo Orden,Nombre, Numero Cliente, Medidor,Ruta,Lect. Actual,Lect. Reactiva, Hora Punta Encontrada, Hora Punta Dejada, Hora, Fecha, Fuera Punta Encontrada, Fuera Punta Dejada, Reset, Nº Sello Encotrado, Nº Sello Dejado, Tarifa, Clave de Lectura, Fecha, Latitud, Longitud, Observación Lector \n"
		fecha = Time.now.strftime("%d%m%Y%H%M%S").to_s
		file = "Visualizacion_lecturas - " +  fecha.to_s + ".csv"
		File.open(file, "w+:UTF-16LE:UTF-8") do |csv|
			csv << header
      @ordenLecturas.each do |orden|
        # Tarifa BT1
        if orden.factor_cobro.nombre == "BT1"
          p orden.codigo
        csv << orden.codigo.to_s + "," +
               orden.cliente.nombre + "," +
               orden.cliente.numero_cliente.to_s + "," +
               orden.medidor.numero_medidor.to_s + "," +
               orden.rutum.codigo.to_s + "," +
               orden.detalle_orden_lectura[0].lectura_actual + ",,,,,,,,,,," +
               orden.factor_cobro.nombre.to_s + "," +
               orden.detalle_orden_lectura.first.clave_lectura.nombre + "," +
               orden.detalle_orden_lectura.first.fecha_ejecucion.strftime('%e/%m/%Y %I:%M:%S %p').to_s + "," +
               orden.gps_latitud.to_s + "," +
               orden.gps_longitud.to_s + "," +
               orden.observacion + "," + "\n"
        end
        # Tarifa BT2 && AT2
        if orden.factor_cobro.nombre == "BT2" || orden.factor_cobro.nombre == "AT2"
          csv << orden.codigo.to_s + "," +
                 orden.cliente.nombre + "," +
                 orden.cliente.numero_cliente.to_s + "," +
                 orden.medidor.numero_medidor.to_s + "," +
                 orden.rutum.codigo.to_s + "," +
                 orden.detalle_orden_lectura[0].lectura_actual + "," +
                 orden.detalle_orden_lectura[1].lectura_actual + ",,,,,,,,,," +
                 orden.factor_cobro.nombre.to_s + "," +
                 orden.detalle_orden_lectura.first.clave_lectura.nombre + "," +
                 orden.detalle_orden_lectura.first.fecha_ejecucion.strftime('%e/%m/%Y %I:%M:%S %p').to_s + "," +
                 orden.gps_latitud.to_s + "," +
                 orden.gps_longitud.to_s + "," +
                 orden.observacion + "," + "\n"
        end
        # Tarifa BT3 && AT3
        if orden.factor_cobro.nombre == "BT3" || orden.factor_cobro.nombre == "AT3"
          csv << orden.codigo.to_s + "," +
                 orden.cliente.nombre + "," +
                 orden.cliente.numero_cliente.to_s + "," +
                 orden.medidor.numero_medidor.to_s + "," +
                 orden.rutum.codigo.to_s + "," +
                 orden.detalle_orden_lectura[0].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[1].lectura_actual.to_s + ",,,,," +
                 orden.detalle_orden_lectura[2].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[3].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[4].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[5].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[6].lectura_actual.to_s + "," +
                 orden.factor_cobro.nombre.to_s + "," +
                 orden.detalle_orden_lectura.first.clave_lectura.nombre + "," +
                 orden.detalle_orden_lectura.first.fecha_ejecucion.strftime('%e/%m/%Y %I:%M:%S %p').to_s + "," +
                 orden.gps_latitud.to_s + "," +
                 orden.gps_longitud.to_s + "," +
                 orden.observacion + "," + "\n"
        end
        # Tarifa BT43 && AT43
        if orden.factor_cobro.nombre == "BT43" || orden.factor_cobro.nombre == "AT43"
          csv << orden.codigo.to_s + "," +
                 orden.cliente.nombre + "," +
                 orden.cliente.numero_cliente.to_s + "," +
                 orden.medidor.numero_medidor.to_s + "," +
                 orden.rutum.codigo.to_s + "," +
                 orden.detalle_orden_lectura[0].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[1].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[2].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[3].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[7].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[6].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[4].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[5].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[8].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[9].lectura_actual.to_s + "," +
                 orden.detalle_orden_lectura[10].lectura_actual.to_s + "," +
                 orden.factor_cobro.nombre.to_s + "," +
                 orden.detalle_orden_lectura.first.clave_lectura.nombre + "," +
                 orden.detalle_orden_lectura.first.fecha_ejecucion.strftime('%e/%m/%Y %I:%M:%S %p').to_s + "," +
                 orden.gps_latitud.to_s + "," +
                 orden.gps_longitud.to_s + "," +
                 orden.observacion + "," + "\n"
        end

			end
		end
		send_file(file, x_sendfile: true, buffer_size: 512, disposition: 'attachment')
  end


end
