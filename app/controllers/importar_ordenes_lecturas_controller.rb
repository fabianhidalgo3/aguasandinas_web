class ImportarOrdenesLecturasController < ApplicationController
  respond_to :js, :html, :json
  require 'csv'

  def index
    @tipolecturas = TipoLectura.all
    ordenes = OrdenLectura.joins(:rutum => [:porcion]).where(porcions: {codigo:[1..31], abierto:true})
    ordenes.each do |orden|
      orden.update(subempresa_id: 1)
    end
  end

  # POST
  # IMPORT CSV
  def import_csv
    row = Array.new
    csv = params[:file]
    contador = 4183
    # porciones = Porcion.where(abierto:true)
    # porciones.each do |porcion|
    #   porcion.update(abierto: false)
    # end

    CSV.foreach(csv.path, headers: true) do |row|
      ##TODO valores fijos aguas andinas
      ##subempresa = 1
      estadoLectura = 1
      numerador = 1
      comunaNombre = row[4].strip
      comuna = Comuna.where(nombre: comunaNombre).first
      p comunaNombre
      porcion = Porcion.where(codigo: row[5].strip.to_i, mes: 11, ano: 2019, zona_id: comuna.zona_id, abierto:true).first
      # # Si no existe la creo
      if porcion.nil?
        porcion = Porcion.create(codigo: row[5].strip.to_i, mes: 11, ano: 2019, zona_id: comuna.zona_id, abierto:true)
      end
      # Busco la ruta
      ruta = Rutum.where(codigo:row[6].strip.to_i, mes: 11, ano: 2019, abierto: true, porcion_id: porcion.id).first
      # Si no existe lo creo
      if ruta.nil?
        ruta = Rutum.create(codigo: row[6].strip.to_i, nombre: row[7].strip, mes: 11, ano: 2019, abierto: true, porcion_id: porcion.id)
      end
      # Busco el cliente
      cliente = Cliente.where(numero_cliente: row[0].strip).first
      #Si No Existe lo Creo
      if cliente.nil?
        cliente = Cliente.create(numero_cliente: row[0].strip, nombre:row[2].strip, direccion: row[3].strip)
      end

      instalacion = Instalacion.where(codigo: row[0].strip).first
      # Si No Existe lo Creo
      if instalacion.nil?
        instalacion = Instalacion.create(codigo: row[0].strip)
      end

      # Busco el Factor de Cobro  
      factorCobro = FactorCobro.where(nombre: row[1]).first
      # Si no existe lo creo
      if factorCobro.nil?
        factorCobro = FactorCobro.create(nombre: row[1].glub(/ /, ''))
      end

      medidor = Medidor.where(numero_medidor: row[8].strip).first
      # Si no existe lo creo
      if medidor.nil?
        medidor = Medidor.create(numero_medidor: row[8].strip, tipo_aparato_id: 1)
      end
      tipoCliente = 1
      ordenLectura = OrdenLectura.create(
        codigo: "AA" + contador.to_s, 
        posicion:row[7].strip,
        cliente_id: cliente.id, 
        rutum_id: ruta.id, 
        tipo_lectura_id: 1, 
        estado_lectura_id: estadoLectura,
        secuencia_lector:row[7].strip, 
        fecha_carga: Time.now, 
        direccion: row[3].strip ,
        direccion_entrega: row[3].strip, 
        relectura:false, 
        comuna_id: comuna.id,
        factor_cobro_id: factorCobro.id, 
        color: row[9], 
        medidor_id: medidor.id, 
        instalacion_id: instalacion.id, 
        num_digitos: 1, 
        tipo_establecimiento_id: 1,
        subempresa_id: 3
      )
      if factorCobro.nombre == "BT1" || factorCobro.nombre == "AT1"
        cantidadLecturas = 1
      end
      if factorCobro.nombre == "AT2" || factorCobro.nombre == "BT2"
        cantidadLecturas = 2
      end
      if factorCobro.nombre == "AT3" || factorCobro.nombre == "BT3"
        cantidadLecturas = 7
      end
      if factorCobro.nombre == "AT42" || factorCobro.nombre == "BT42"
        cantidadLecturas = 9
      end
      if factorCobro.nombre == "AT43" || factorCobro.nombre == "BT43"
        cantidadLecturas = 11
      end
      iterator = 0
      cantidadLecturas.times do |n|
        # Creo el detalle de la orden de lectura...
        detalleOrdenLectura = DetalleOrdenLectura.create(orden_lectura_id: ordenLectura.id,lectura_anterior: 0,rango_superior: 0,rango_inferior: 0,lectura_promedio: 0,numerador_id:  numerador, tipo_consumo_id: 1)
      end
      contador+=1
    end
  end
end
