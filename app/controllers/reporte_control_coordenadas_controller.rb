class ReporteControlCoordenadasController < ApplicationController
	respond_to :js, :html, :json, :pdf
	# => Control de Coordenadas GPS
	def index
		# Busca usuario y zonas y porcion del usuario logueado
		user = current_usuario
    @zonas = user.empleado.zona
	end

	# => Carga Empleados
	def carga_empleados
		#Busca los empleados  por zona y perfil 6, 7
		@empleados = Empleado.joins(:usuario).where(usuarios: {perfil_id: [6..7]})
    respond_with @empleados
	end

	# => Carga Rutas
	def carga_rutas
		@rutas = Rutum.where(porcion_id: params[:porcion], abierto: true)
		respond_with @rutas
	end

	# GET
	# DEVUELVE @PORCIONES
	def carga_porciones
		@porciones = Porcion.where(zona_id: params[:zona], abierto: true).order(:codigo)
		respond_with @porciones
	end


	# > GET
	# > Tabla contenido
	def carga_filtro
		if !params[:empleado].blank?
			empleados = Empleado.find(params[:empleado].to_i)
		else
			empleados = Empleado.all
		end
		if !params[:porcion].blank?
			porciones = Porcion.find(params[:porcion].to_i)
		else
			porciones = Porcion.where(abierto:true)
		end
		@listaRsiete = Asignacion.where(empleado_id: empleados).joins(:porcion, :orden_lectura).where(porcions: {id: porciones },orden_lecturas:{estado_lectura_id: [4,5]} )
		@listaRsiete = @listaRsiete.paginate(:page => params[:page], :per_page => 10)
		respond_with @listaRsiete
	end

	# GET 
	# Devuelve @hash con coordenadas GPS
	def carga_ubicacion
		ubicaciones = Array.new
	  latitud = params[:latitud]
	  longitud = params[:longitud]
	  ubicaciones.push([latitud.to_s, longitud.to_s, ""])
	  @hash = Gmaps4rails.build_markers(ubicaciones) do |u, marker|
	    marker.lat u[0]
	    marker.lng u[1]
	    marker.infowindow u[2]
		end
		p @hash
		respond_with @hash
	end

	# Exportar Csv
	def exportar_csv
		georeferencia = params[:georeferencia].to_i
		if (georeferencia == 0) then
			#Lista de asignaciones
			@listaRsiete = Asignacion.where(empleado_id: params[:empleado] ).joins(:porcion, :orden_lectura).where(porcions: { zona_id: params[:zona], id: params[:porcion] },
                                     orden_lecturas:{estado_lectura_id: [4,5]} )
		end
		if (georeferencia == 1) then
			@listaRsiete = Asignacion.where(porcion_id: params[:porcion]).joins(:porcion, :orden_lectura).where(porcions: { zona_id: params[:zona], id: params[:porcion] },
                                     orden_lecturas:{estado_lectura_id: [4,5], gps_latitud: "0.0", gps_longitud: "0.0"} )
		end
		header = "Región, Comuna, Lector, Ruta,Numero Cliente,Medidor ,Fecha Lectura, Hora Lectura,Clave Lectura, Latitud, Longitud, Observación Lector\n"
    	fecha = Time.now
    	file = "control_gps - " +  fecha.to_s + ".csv"

    	File.open(file, "w+:UTF-16LE:UTF-8") do |csv|
     		csv << header
      		@listaRsiete.each do |l|
						csv << l.porcion.zona.nombre + "," + 
									 l.orden_lectura.comuna.nombre + "," + 
									 l.empleado.nombre_completo + "," + 
									 l.rutum.codigo + "," +
									 l.orden_lectura.cliente.numero_cliente.to_s + "," + 
									 l.orden_lectura.medidor.numero_medidor.to_s + "," +  
									 l.orden_lectura.detalle_orden_lectura.first.fecha_ejecucion.strftime("%e/%m/%Y ") + "," +
									 l.orden_lectura.detalle_orden_lectura.first.fecha_ejecucion.strftime("%H:%M:%S")+ "," +
									 l.orden_lectura.detalle_orden_lectura.first.clave_lectura.nombre + "," +
									 l.orden_lectura.gps_latitud.to_s+ "," +
									 l.orden_lectura.gps_longitud.to_s+ "," +
        		       l.orden_lectura.observacion.to_s+ "\n"
      		end
    	end
    	send_file(file, x_sendfile: true, buffer_size: 512)
	end

	# GET
	# Exportar PDF
	def exportar_pdf
		georeferencia = params[:georeferencia].to_i
		if (georeferencia == 0) then
			#Lista de asignaciones
			@listaRsiete = Asignacion.where(empleado_id: params[:empleado]).joins(:porcion,:orden_lectura).where(porcions: { zona_id: params[:zona], id: params[:porcion] },
                                     orden_lecturas:{estado_lectura_id: [4,5]} )
		end
		if (georeferencia == 1) then
			@listaRsiete = Asignacion.where(porcion_id: params[:porcion]).joins(:porcion, :orden_lectura).where(porcions: { zona_id: params[:zona], id: params[:porcion] },
                                     orden_lecturas:{estado_lectura_id: [4,5], gps_latitud: nil, gps_longitud: nil} )
		end
		fecha = Time.now.strftime("%d%m%Y%H%M%S").to_s
		nombreArchivo = "reporte_efectividad_porcion - " +  fecha.to_s + ".pdf"
		respond_to do |format|
			format.pdf do
        pdf = CoordenadasGpsPdf.new(@listaRsiete)
        send_data pdf.render, filename: nombreArchivo, type: 'application/pdf'
			end
		end
	end
end
