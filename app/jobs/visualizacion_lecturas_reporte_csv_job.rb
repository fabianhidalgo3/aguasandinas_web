class VisualizacionLecturasReporteCsvJob < ApplicationJob
  queue_as :default

  def perform(mes)
    rutas = Rutum.joins(:porcion).where(porcions: {mes: params[:mes].to_i})
    ordenLecturas = OrdenLectura.where(estado_lectura_id: 4, tipo_lectura_id: 1, rutum_id: rutas).joins(:detalle_orden_lectura).group(:id).order("detalle_orden_lecturas.fecha_ejecucion DESC")
    header = "Codigo Orden,Nº Cliente, Nombre, Dirección, Comuna, Zona, Grupo, Libreta, Correlativo,Medidor,Lectura Actual,Tarifa, Clave de Lectura, Fecha,Observación Lector, Latitud, Longitud \n"
		fecha = Time.now.strftime("%d%m%Y%H%M%S").to_s
		file = "Visualizacion_lecturas - " +  fecha.to_s + ".csv"
		File.open(file, "w+:UTF-16LE:UTF-8") do |csv|
			csv << header
			@ordenLecturas.each do |l|
        csv << l.codigo.to_s + "," + l.cliente.numero_cliente.to_s + "," + l.cliente.nombre + ","  + l.cliente.direccion + ","  + l.comuna.nombre + "," + l.rutum.porcion.zona.nombre.to_s + "," +
               l.rutum.porcion.codigo.to_s + "," + l.rutum.codigo.to_s + "," + l.secuencia_lector.to_s + "," +
							 l.medidor.numero_medidor.to_s + "," + l.detalle_orden_lectura.first.lectura_actual + "," + l.factor_cobro.nombre.to_s + "," + l.detalle_orden_lectura.first.clave_lectura.nombre + "," +
							 l.detalle_orden_lectura.first.fecha_ejecucion.strftime('%e/%m/%Y %I:%M:%S %p').to_s + "," + l.observacion.to_s + "," + l.gps_latitud.to_s + "," +
							 l.gps_longitud.to_s + "," + "\n"
			end
		end
    send_file(file, x_sendfile: true, buffer_size: 512, disposition: 'attachment')
  end
end
