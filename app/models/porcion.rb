class Porcion < ApplicationRecord
  belongs_to :zona
  belongs_to :subempresa
  belongs_to :rutum
  has_many :asignacion
  has_many :orden_lectura

end
