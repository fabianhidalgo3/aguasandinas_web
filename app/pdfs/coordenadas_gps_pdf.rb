class CoordenadasGpsPdf < Prawn::Document
  include Prawn::View
  require 'prawn-styled-text'

  def initialize(lista)
    super :page_size => "A4", :page_layout => :landscape
    @lista = lista
    fuentes
    header
    body
    footer 
  end

  def header    
    image "#{Rails.root}/public/images/aguas_andinas_logo.png",:position => :right,  :width => 90, :heigth => 30
    styled_text '<h4 style="text-align: left;"><b>REPORTE:</b> COORDENADAS GPS </h4> <hr>'
  end
  

  def body
    move_down 20
    cant_lista = lista.count
    table lista do
      row(0).size = 11
      row(0).background_color = '555658'
      row(0).text_color = 'ffffff'
      row(0).font_style = :bold
      columns(0..10).align = :center
      columns(0..3).font_style = :bold
      self.header = true
      self.cells.borders = [:top, :bottom, :left, :right]
      self.column_widths = [60,60,70,60,90,110,80,90,70]
    end
  end
  

  def footer
    move_down 40
    styled_text '<p style="text-align:right">' + "Fecha descarga: " + Time.now.strftime("%d/%m/%Y %H:%M:%S").to_s + "</p>" 
  end

  def lista
    [['Comuna', 'Lector', 'Ruta', 'Número Cliente', 'Nombre', 'Dirección', 'Fecha Lectura', 'Hora Visitada', 'Latitud','Longitud']] +
    @lista.map do |l|
      if  !l.orden_lectura.detalle_orden_lectura.first.fecha_ejecucion.blank?
       fecha =  l.orden_lectura.detalle_orden_lectura.first.fecha_ejecucion.strftime("%e/%m/%Y ")
      else    
        fecha = ""
      end
      if  !l.orden_lectura.detalle_orden_lectura.first.fecha_ejecucion.blank?
        hora = l.orden_lectura.detalle_orden_lectura.first.fecha_ejecucion.strftime("%H:%M:%S")
      else
        hora = ""
      end
      [l.orden_lectura.comuna.nombre,
       l.empleado.nombre_completo,
       l.rutum.codigo_nombre,
       l.orden_lectura.cliente.numero_cliente,
       l.orden_lectura.cliente.nombre,
       l.orden_lectura.direccion,
       fecha,
       hora,
       l.orden_lectura.gps_latitud,
       l.orden_lectura.gps_longitud]
    end
  end

  def fuentes
    font_families.update("Roboto" => {
      :normal => "public/assets/fonts/Roboto-Regular.ttf",
      :italic => "public/assets/fonts/Roboto-Italic.ttf",
      :bold => "public/assets/fonts/Roboto-Bold.ttf",
    })
    font "Roboto"
  end
  
end  