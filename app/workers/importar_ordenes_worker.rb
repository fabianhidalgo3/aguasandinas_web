class ImportarOrdenesWorker < ApplicationWorker
  require 'csv'
  row = Array.new

  def perform(csv_path)
    CSV.foreach((csv_path), headers: true) do |row|
      # TODO Valores Fijos Empresa Chilquinta
      subempresa = 1
      estadoLectura = 1
      numerador = 1
      contador = 0

      #Proceso lecturas
      procesoLecura = ProcesoLectura.where(codigo:row[0]).first
      if procesoLecura.nil?
        procesoLecura = ProcesoLectura.create(codigo: row[0])
      end

      #zona = Zona.where(nombre: row[10]).first
      #Busco Comunas Precargadas
      comuna = Comuna.where(nombre: row[9]).first
      # Busco la porcion
      porcion = Porcion.where(codigo: row[4], mes: row[2], ano: row[1], zona_id: comuna.zona_id).first
      # Si no existe la creo
      if porcion.nil?
        porcion = Porcion.create(codigo: row[4], mes: row[2], ano:row[1], zona_id: comuna.zona_id, abierto:true)
      end

      # Busco la ruta
      ruta = Rutum.where(codigo: row[12], mes: row[2], ano:row[1]).first
      # Si no existe lo creo
      if ruta.nil?
        ruta = Rutum.create(codigo: row[12], mes: row[2], ano:row[1], abierto:true, porcion_id: porcion.id)
      end

      # Busco el cliente
      cliente = Cliente.where(numero_cliente: row[5]).first
      # Si No Existe lo Creo
      if cliente.nil?
        cliente = Cliente.create(numero_cliente: row[5], nombre:row[16], direccion: row[11])
      end

      # Busco Instalación
      instalacion = Instalacion.where(codigo: row[5]).first
      # Si no Existe la creo
      if instalacion.nil?
        instalacion = Instalacion.create(codigo: row[5])
      end
      
      tipoAparato = TipoAparato.where(nombre: row[8]).first
      if tipoAparato.nil?
        tipoAparato = TipoAparato.create(nombre:row[8])
      end

      # Busco el Factor de Cobro
      factorCobro = FactorCobro.where(nombre: row[15]).first
      # Si no existe lo creo
      if factorCobro.nil?
        factorCobro = FactorCobro.create(nombre: row[15])
      end

      # Busco el tipo consumo
      tipoConsumo = TipoConsumo.where(nombre: row[17]).first
      # Si no existe lo creo
      if tipoConsumo.nil?
        tipoConsumo = TipoConsumo.create(nombre: row[17], descripcion: row[18])
      end

      medidor = Medidor.where(numero_medidor: row[7]).first
      # Si no existe lo creo
      if medidor.nil?
        medidor = Medidor.create(numero_medidor: row[7], tipo_aparato_id: tipoAparato.id)
      end

      tipoCliente = TipoCliente.where(nombre: row[25]).first
      if tipoCliente.nil?
        tipoCliente = TipoCliente.create(nombre: row[25])
      end

      tipoEstablecimiento = TipoEstablecimiento.where(nombre: row[26]).first
      if tipoEstablecimiento.nil?
        tipoEstablecimiento = TipoEstablecimiento.create(nombre: row[26])
      end
      # Creo Orden de lectura...
      # TODO: Agregar Codigo pendiente de envio para validar ordenes repetidas
      # TODO: Modificar a a varios detalles por orden lectura

      # Valido la prioridad del cliente
      if row[24].blank?
        color = 5
      else
        color = row[24]
      end
      # Valido lectura anterior nula
      if row[20].blank?
        lectura_anterior = 0
      else
        lectura_anterior = row[20]
      end
      # Valido Rango superior
      if row[23].blank?
        rango_superior = 0
      else
        rango_superior = row[23]
      end
      # Valido Rango inferior
      if row[21].blank?
        rango_inferior = 0
      else
        rango_inferior = row[21]
      end
      # Valido Lectura Promedio
      if row[22].blank?
        lectura_promedio = 0
      else
        lectura_promedio = row[22]
      end
      codigo = row[3] + "-" + medidor.numero_medidor
      ordenLectura = OrdenLectura.where(codigo: codigo).first

      if ordenLectura.nil?
        ordenLectura = OrdenLectura.create(
                                    codigo: codigo,
                                    posicion:row[13],
                                    instalacion_id: instalacion.id,
                                    cliente_id:cliente.id,
                                    rutum_id: ruta.id,
                                    tipo_lectura_id: 1,
                                    estado_lectura_id: estadoLectura,
                                    secuencia_lector:row[13],
                                    fecha_carga: Time.now,
                                    direccion: row[11] ,
                                    direccion_entrega: row[11],
                                    relectura:false,
                                    comuna_id: comuna.id,
                                    factor_cobro_id: factorCobro.id,
                                    color: color,
                                    num_digitos:row[14],
                                    proceso_lecturas_id: procesoLecura.id,
                                    medidor_id: medidor.id,
                                    tipo_cliente_id: tipoCliente.id,
                                    tipo_establecimiento_id: tipoEstablecimiento.id,
                                    orden_mandante: row[3])


            # Creo el detalle de la orden de lectura...
        detalleOrdenLectura = DetalleOrdenLectura.create(
                                    orden_lectura_id: ordenLectura.id,
                                    tipo_consumo_id: tipoConsumo.id,
                                    lectura_anterior: lectura_anterior,
                                    rango_superior: rango_superior,
                                    rango_inferior: rango_inferior,
                                    lectura_promedio: lectura_promedio,
                                    numerador_id:  numerador,
                                    fecha_lectura_anterior: row[19]
        )
      else
        # Creo el detalle de la orden de lectura...
        detalleOrdenLectura = DetalleOrdenLectura.create(
                                      orden_lectura_id:ordenLectura.id,
                                      tipo_consumo_id: tipoConsumo.id,
                                      lectura_anterior:lectura_anterior,
                                      rango_superior: rango_superior,
                                      rango_inferior: rango_inferior,
                                      lectura_promedio: lectura_promedio,
                                      numerador_id:  numerador,
                                      fecha_lectura_anterior: row[19]
        )
        contador = 0
      end
    end
  end
end