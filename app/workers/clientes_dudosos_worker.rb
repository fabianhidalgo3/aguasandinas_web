class ClientesDudososWorker < ApplicationWorker
  def perform(zona, proceso, porcion)
    @lista = Array.new
    rutas = Rutum.where(porcion_id: porcion)
    porciones = Porcion.where(zona_id: zona)
    detalle = DetalleOrdenLectura.joins(:clave_lectura, :orden_lectura).where(
        clave_lecturas: {efectivo: true}, orden_lecturas: {rutum_id: rutas, estado_lectura_id: 4,
                                                       autorizado_facturacion: false, tipo_lectura_id: proceso,
                                                       relectura: false}).order("fecha_ejecucion DESC")

    detalle.each do |d|
      asignacion = d.orden_lectura.asignacion
      lectura_anterior = d.lectura_anterior
      lectura_actual = d.lectura_actual
      if lectura_actual.nil?
        lectura_actual =0
      else
        lectura_actual =d.lectura_actual
      end
      lectura_promedio = d.lectura_promedio
      lectura_esperada_maxima = d.rango_superior#(d.lectura_anterior + d.lectura_promedio) * 1.05
      lectura_esperada_minima = d.rango_inferior#(d.lectura_anterior + d.lectura_promedio) * 0.95

      consumo = (d.lectura_actual - d.lectura_anterior)
      clave_lectura =  d.clave_lectura.nombre


      if !d.lectura_promedio.nil? then
        lectura_promedio = d.lectura_promedio
      else
        lectura_promedio = 0
      end
      # > Lectura Maxima
      if (lectura_esperada_maxima < lectura_actual) then
         @lista.push([d.orden_lectura.cliente.nombre, #0
                      d.orden_lectura.cliente.numero_cliente,  #1
                      d.orden_lectura.rutum.codigo, #2
                      asignacion.empleado.nombre_completo,  #3
                      lectura_promedio,#4
                      lectura_anterior, #5
                      lectura_actual, #6
                      consumo,  #7
                      lectura_esperada_maxima.to_i,#8
                      clave_lectura, #0
                      "", #10
                      d.orden_lectura.id,  #11
                      d.fotografium.blank?, #12
                      d.orden_lectura.factor_cobro.nombre, #13
                      d.orden_lectura.medidor.numero_medidor, #14
                      d.fecha_ejecucion.strftime("%H:%M:%S %d/%m/%Y")]) #15
      end
      # > Lectura Minima
      if (lectura_esperada_minima > lectura_actual) then
        @lista.push([d.orden_lectura.cliente.nombre,#0
                     d.orden_lectura.cliente.numero_cliente,#1
                     d.orden_lectura.rutum.codigo,#2
                     asignacion.empleado.nombre_completo,#3
                     lectura_promedio, #4
                     lectura_anterior,#5
                     lectura_actual,#6
                     consumo,#7
                     lectura_esperada_minima.to_i,#8
                     clave_lectura,#9
                     "",#10
                     d.orden_lectura.id,#11
                     d.fotografium.blank?,#12
                     d.orden_lectura.factor_cobro.nombre,#13
                     d.orden_lectura.medidor.numero_medidor,#14
                     d.fecha_ejecucion.strftime("%H:%M:%S %d/%m/%Y")])#15
      end
    end
   #@lista = @lista.paginate(:page => params[:page], :per_page => 20)
   #respond_with @lista
  end
end