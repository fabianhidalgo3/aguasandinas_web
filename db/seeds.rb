
p "Creando Región y Provincias de Región de Valparaiso"
Region.create(id:5, codigo: '05',nombre: 'Región  Metropolitana')

creando = "CREANDO OFICINA DE"
p creando + "SANTIAGO"
Zona.create(id: 1, nombre:"SANTIAGO", region_id: 5)
Comuna.create(codigo: '13101', nombre: 'Santiago', zona_id: 1)
Comuna.create(codigo: '13102', nombre: 'Cerrillos', zona_id: 1)
Comuna.create(codigo: '13103', nombre: 'Cerro Navia', zona_id: 1)
Comuna.create(codigo: '13104', nombre: 'Conchali', zona_id: 1)
Comuna.create(codigo: '13105', nombre: 'El Bosque', zona_id: 1)
Comuna.create(codigo: '13106', nombre: 'Estacion Central', zona_id: 1)
Comuna.create(codigo: '13107', nombre: 'Huechuraba', zona_id: 1)
Comuna.create(codigo: '13108', nombre: 'Independencia', zona_id: 1)
Comuna.create(codigo: '13109', nombre: 'La Cisterna', zona_id: 1)
Comuna.create(codigo: '131010', nombre: 'La Florida', zona_id: 1)
Comuna.create(codigo: '131011', nombre: 'La Granja', zona_id: 1)
Comuna.create(codigo: '131012', nombre: 'La Pintana', zona_id: 1)
Comuna.create(codigo: '131013', nombre: 'La Reina', zona_id: 1)
Comuna.create(codigo: '131014', nombre: 'Las Condes', zona_id: 1)
Comuna.create(codigo: '131015', nombre: 'Lo Barnechea', zona_id: 1)
Comuna.create(codigo: '131016', nombre: 'Lo Espejo', zona_id: 1)
Comuna.create(codigo: '131017', nombre: 'Lo Prado', zona_id: 1)
Comuna.create(codigo: '131018', nombre: 'Macul', zona_id: 1)
Comuna.create(codigo: '131019', nombre: 'Maipu', zona_id: 1)
Comuna.create(codigo: '131020', nombre: 'Ñuñoa', zona_id: 1)
Comuna.create(codigo: '131021', nombre: 'Pedro Aguirre Cerda', zona_id: 1)
Comuna.create(codigo: '131022', nombre: 'Peñalolen', zona_id: 1)
Comuna.create(codigo: '131023', nombre: 'Providencia', zona_id: 1)
Comuna.create(codigo: '131024', nombre: 'Pudahuel', zona_id: 1)
Comuna.create(codigo: '131025', nombre: 'Quilicura', zona_id: 1)
Comuna.create(codigo: '131026', nombre: 'Quinta Normal', zona_id: 1)
Comuna.create(codigo: '131027', nombre: 'Recoleta', zona_id: 1)
Comuna.create(codigo: '131028', nombre: 'Renca', zona_id: 1)
Comuna.create(codigo: '131029', nombre: 'San Joaquin', zona_id: 1)
Comuna.create(codigo: '131030', nombre: 'San Miguel', zona_id: 1)
Comuna.create(codigo: '131031', nombre: 'San Ramon', zona_id: 1)
Comuna.create(codigo: '131032', nombre: 'Vitacura', zona_id: 1)
Comuna.create(codigo: '13201', nombre: 'Punte Alto', zona_id: 1)
Comuna.create(codigo: '13202', nombre: 'Pirque', zona_id: 1)
Comuna.create(codigo: '13203', nombre: 'San Jose de Maipo', zona_id: 1)
Comuna.create(codigo: '13301', nombre: 'Colina', zona_id: 1)
Comuna.create(codigo: '13302', nombre: 'Lampa', zona_id: 1)
Comuna.create(codigo: '13303', nombre: 'Til til', zona_id: 1)
Comuna.create(codigo: '13401', nombre: 'San Bernardo', zona_id: 1)
Comuna.create(codigo: '13402', nombre: 'Buin', zona_id: 1)
Comuna.create(codigo: '13403', nombre: 'Calera de Tango', zona_id: 1)
Comuna.create(codigo: '13404', nombre: 'Paine', zona_id: 1)
Comuna.create(codigo: '13501', nombre: 'Melipilla', zona_id: 1)
Comuna.create(codigo: '13502', nombre: 'Alhue', zona_id: 1)
Comuna.create(codigo: '13503', nombre: 'Curacavi', zona_id: 1)
Comuna.create(codigo: '13504', nombre: 'Maria Pinto', zona_id: 1)
Comuna.create(codigo: '13505', nombre: 'San Pedro', zona_id: 1)
Comuna.create(codigo: '13601', nombre: 'Talagante', zona_id: 1)
Comuna.create(codigo: '13602', nombre: 'El Monte', zona_id: 1)
Comuna.create(codigo: '13603', nombre: 'Isla de Maipo', zona_id: 1)
Comuna.create(codigo: '13604', nombre: 'Padre Hurtado', zona_id: 1)
Comuna.create(codigo: '13605', nombre: 'Peñaflor', zona_id: 1)

p "Creando contratistas"
Contratistum.create(id:1, nombre:'PROVIDER')

puts "Creando tipos de lecturas"
TipoLectura.create(id: 1,nombre:'Lectura Normal')
TipoLectura.create(id: 2,nombre:'Verificación')

p "Creando perfiles de usuario"
Perfil.create(id: 1, nombre: 'Admininistrador del Sistema')
Perfil.create(id: 2, nombre: 'Administrador de Contratos')
Perfil.create(id: 3, nombre: 'Coordinador de Contratos')
Perfil.create(id: 4, nombre: 'Analista de Lecturas')
Perfil.create(id: 5, nombre: 'Atencion al Cliente')
Perfil.create(id: 6, nombre: 'Agente de Terreno')
Perfil.create(id: 7, nombre: 'Inspector Terreno')

puts "Creando empresa"
Empresa.create(rut:'11.111.111-1', razon_social: 'Aguas Andinas', giro:'Servicios Agua', direccion:'')

puts "Creando Subempresas"
Subempresa.create(rut:'11.111.111-1', razon_social: 'Aguas Andinas', giro:'Servicios Agua', direccion:'')

puts "Vinculando Contratista Subempresa"
ContratistaSubempresa.create(contratistum_id: 1, subempresa_id: 1)

p "Creando Contratista Zona"
ContratistaZona.create(contratista_id: 1, zona_id: 1)

p "Vinculando SubEmpresa con Zonas"
SubempresasZonas.create(zona_id:1, subempresa_id: 1)

puts "Vinculando empresa con subempresas"
EmpresaSubempresa.create(subempresa_id:1, empresa_id:1)

puts "Creando estados de lectura"
EstadoLectura.create(nombre:'Sin Asignar')
EstadoLectura.create(nombre:'Asignado')
EstadoLectura.create(nombre:'En Smartphone')
EstadoLectura.create(nombre:'Visitado')
EstadoLectura.create(nombre:'A Facturación')

p "Creando marcas"
Marca.create(nombre: 'Motorola')
Marca.create(nombre: 'Hawei')
Marca.create(nombre: 'Intermec')
Marca.create(nombre: 'Samsung')

puts "Creando modelos de celular"
Modelo.create(nombre: 'Moto Maxx', marca_id: 1)
Modelo.create(nombre: 'Moto G 3', marca_id: 1)
Modelo.create(nombre: 'Moto G 4', marca_id: 1)
Modelo.create(nombre: 'Moto X', marca_id: 1)
Modelo.create(nombre: 'Hawei P9 Lite', marca_id: 2)
Modelo.create(nombre: 'Hawei P8 Lite', marca_id: 2)
Modelo.create(nombre: 'Hawei P8', marca_id: 2)
Modelo.create(nombre: 'Hawei P9', marca_id: 2)
Modelo.create(nombre: 'Hawei P10', marca_id: 2)
Modelo.create(nombre: 'Hawei MATE 9', marca_id: 2)
Modelo.create(nombre: 'Hawei Y6 II', marca_id: 2)
Modelo.create(nombre: 'Intermec CN51', marca_id: 3)
Modelo.create(nombre: 'Intermec CN50', marca_id: 3)
Modelo.create(nombre: 'Intermec CS40', marca_id: 3)
Modelo.create(nombre: 'Intermec CK70', marca_id: 3)
Modelo.create(nombre: 'Samsung J1', marca_id: 4)
Modelo.create(nombre: 'Samsung J2', marca_id: 4)
Modelo.create(nombre: 'Samsung J5', marca_id: 4)
Modelo.create(nombre: 'Samsung J7', marca_id: 4)
Modelo.create(nombre: 'Samsung J7 Prime', marca_id: 4)

p "Creando usuarios Admin del Sistema"
Usuario.create(id: 1, email: 'deploy', password: 'P@ssw0rd666', perfil_id: 1)
Usuario.create(id: 2, email: 'jfcorail', password: 'jfcorail2018', perfil_id: 1)

p "Creando empleados Admin del Sistema"
Empleado.create(id: 1, nombre:'Fabián Andrés', apellido_paterno:'Hidalgo', apellido_materno: 'Neira', rut:'18.681.243-3', contratistum_id: 1,subempresa_id:1, usuario_id:1)
Empleado.create(id: 2, nombre:'José Fernando', apellido_paterno:'Corail', apellido_materno: 'Moreno', rut:'11.620.739-7', contratistum_id: 1,subempresa_id:1, usuario_id:2)

puts "Creo Admin Plan Piloto"
Usuario.create(id: 3,  email: 'aguasandinas', password:'aguasandinas', perfil_id: 1)
Empleado.create(id: 3, nombre:'Aguas Andinas', rut:'96722460-K', contratistum_id: 1, subempresa_id:1, usuario_id:3)

puts "Vinculando empleados con zonas"
EmpleadosZonas.create(empleado_id:1, zona_id:1) #Deploy
EmpleadosZonas.create(empleado_id:2, zona_id:1) #Jose Corail
EmpleadosZonas.create(empleado_id:3, zona_id:1) #Aguas Andinas

p "Numerador"
Numerador.create(nombre: "No Aplica")
Instalacion.create(codigo: "No Aplica")
TipoAparato.create(nombre: "No Aplica")
TipoConsumo.create(nombre: "No Aplica")
TipoEstablecimiento.create(nombre: "No Aplica")


p "Observacion medidor"
ObservacionMedidor.create(descripcion:"Sin Observación")
ObservacionMedidor.create(descripcion: "Dentro de la casa")
ObservacionMedidor.create(descripcion: "En antejardín, no se lee")
ObservacionMedidor.create(descripcion: "En antejardín, se lee")
ObservacionMedidor.create(descripcion: "En fachada, libre acceso")
ObservacionMedidor.create(descripcion: "En altura, no se lee")
ObservacionMedidor.create(descripcion: "En bateria, se lee")




