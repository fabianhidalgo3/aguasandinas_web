class AddObservacionMedidorToOrdenLectura < ActiveRecord::Migration[5.0]
  def change
    add_reference :orden_lecturas, :observacion_medidor, foreign_key: true
  end
end
