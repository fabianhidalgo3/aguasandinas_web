class AddVerficiacionToClaveLecturas < ActiveRecord::Migration[5.0]
  def change
    add_column :clave_lecturas, :verificacion, :boolean
    add_column :clave_lecturas, :foto, :boolean
  end
end
