class AddObservacionRecintoToCliente < ActiveRecord::Migration[5.0]
  def change
    add_column :clientes, :observacion_recinto, :string
  end
end
