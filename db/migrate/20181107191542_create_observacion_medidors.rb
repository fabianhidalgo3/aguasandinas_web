class CreateObservacionMedidors < ActiveRecord::Migration[5.0]
  def change
    create_table :observacion_medidors do |t|
      t.string :descripcion

      t.timestamps
    end
  end
end
