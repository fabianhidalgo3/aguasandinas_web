class AddVerificadoToCliente < ActiveRecord::Migration[5.0]
  def change
    add_column :clientes, :verificado, :boolean
  end
end
