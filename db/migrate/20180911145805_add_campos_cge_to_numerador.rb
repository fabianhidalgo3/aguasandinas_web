class AddCamposCgeToNumerador < ActiveRecord::Migration[5.0]
  def change
    add_column :numeradors, :secuencia, :integer
    add_column :numeradors, :descripcion, :string
  end
end
