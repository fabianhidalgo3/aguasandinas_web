class AddSubempresaToOrdenLectura < ActiveRecord::Migration[5.0]
  def change
    add_reference :orden_lecturas, :subempresa, foreign_key: true
  end
end
