class AddRegionToZona < ActiveRecord::Migration[5.0]
  def change
    add_reference :zonas, :region, foreign_key: true
  end
end
