class AddConsumoDudosoToOrdenLectura < ActiveRecord::Migration[5.0]
  def change
    add_column :orden_lecturas, :consumo_dudoso, :boolean
  end
end
