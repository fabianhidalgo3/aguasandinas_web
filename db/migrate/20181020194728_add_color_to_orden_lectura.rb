class AddColorToOrdenLectura < ActiveRecord::Migration[5.0]
  def change
    add_column :orden_lecturas, :color, :integer
  end
end
