class AddNumDigitosToOrdenLectura < ActiveRecord::Migration[5.0]
  def change
    add_column :orden_lecturas, :num_digitos, :integer
  end
end
