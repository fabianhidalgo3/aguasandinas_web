class AddRefAparatoToMedidor < ActiveRecord::Migration[5.0]
  def change
    add_reference :medidors, :tipo_aparato, foreign_key: true
  end
end
