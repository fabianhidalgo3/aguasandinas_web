class AddFotoToObservacione < ActiveRecord::Migration[5.0]
  def change
    add_column :observaciones, :foto, :boolean
  end
end
