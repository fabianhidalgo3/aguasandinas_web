class AddCamposCgeToDetalleOrdenLectura < ActiveRecord::Migration[5.0]
  def change
    add_column :detalle_orden_lecturas, :hora_lectora, :time
    add_column :detalle_orden_lecturas, :hora_medidor, :time
    add_column :detalle_orden_lecturas, :energia_reactiva, :decimal
    add_column :detalle_orden_lecturas, :energia_suministrada, :decimal
    add_column :detalle_orden_lecturas, :fecha_energia_suministrada, :date
    add_column :detalle_orden_lecturas, :hora_energia_suministrada, :time
    add_column :detalle_orden_lecturas, :reset_energia_suministrada, :integer
    add_column :detalle_orden_lecturas, :energia_punta, :decimal
    add_column :detalle_orden_lecturas, :fecha_energia_punta, :date
    add_column :detalle_orden_lecturas, :hora_energia_punta, :time
    add_column :detalle_orden_lecturas, :reset_energia_punta, :integer
    add_column :detalle_orden_lecturas, :energia_inyectada, :decimal
  end
end
