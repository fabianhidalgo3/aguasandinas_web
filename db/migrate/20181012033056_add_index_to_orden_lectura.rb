class AddIndexToOrdenLectura < ActiveRecord::Migration[5.0]
  def change
    add_index :orden_lecturas, :codigo
  end
end
