class AddTipoConsumoToOrdenLectura < ActiveRecord::Migration[5.0]
  def change
    add_reference :detalle_orden_lecturas, :tipo_consumo, foreign_key: true
  end
end
